package ru.roman.other;

import java.util.Arrays;
import java.util.StringJoiner;

public class BubbleSort {

    public static void main(String[] args) {

        int[] array = {6, 1, 0, 1, 8, 32, 4};
        boolean done = false;
        for (int j = 0; j < array.length; j++) {
            done = true;
            for (int i = 0; i < array.length - j - 1; i++) {
                if (array[i] > array[i + 1]) {
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    done = false;
                }
            }
            if (done) {
                break;
            }
        }

        System.out.println(Arrays.toString(array));

        System.out.println(Arrays.stream(array)
                .mapToObj(String::valueOf)
                .reduce((left, right) -> left + ", " + right)
                .get());
    }


}
