package ru.roman.year2022.day1;

import org.apache.commons.lang3.StringUtils;
import ru.roman.common.utils.FileReader;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        List<String> lines = FileReader.readFile("2022/task1.txt");
        System.out.println(lines);
        System.out.println(new Runner().findTheMostCalories(lines));
        System.out.println(new Runner().findThreeMostCalories(lines));
    }

    public int findTheMostCalories(List<String> lines) {
        List<Integer> results = getList(lines);
        return results.stream().max(Comparator.naturalOrder()).orElse(0);
    }

    public long findThreeMostCalories(List<String> lines) {
        List<Integer> results = getList(lines);
        return results.stream().sorted(Comparator.reverseOrder()).limit(3).reduce(Integer::sum).orElse(0);
    }

    private List<Integer> getList(List<String> lines) {
        List<Integer> results = new ArrayList<>();
        int sum = 0;
        for (String line : lines) {
            if (StringUtils.isBlank(line)) {
                results.add(sum);
                sum = 0;
            } else {
                sum += Integer.parseInt(line);
            }
        }
        return results;
    }
}
