package ru.roman.year2022.day3;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import ru.roman.common.utils.FileReader;

public class Runner {

    private static Map<Character, Integer> map = new HashMap<>();

    public static void main(String[] args) {

        int i = 0;
        for (char letter = 'a'; letter <= 'z'; letter++) {
            i++;
            map.put(letter, i);
        }
        for (char letter = 'A'; letter <= 'Z'; letter++) {
            i++;
            map.put(letter, i);
        }

        List<String> lines = FileReader.readFile("2022/task3.txt");
        int result = lines.stream()
                .mapToInt(Runner::calculateLine)
                .sum();
        System.out.println(result);
        // part 2
        int sum = IntStream.iterate(0, operand -> operand + 3)
                .limit(lines.size() / 3)
                .map(line -> {
                    Set<Integer> first = lines.get(line).chars().boxed().collect(Collectors.toSet());
                    Set<Integer> second = lines.get(line + 1).chars().boxed().collect(Collectors.toSet());
                    Set<Integer> third = lines.get(line + 2).chars().boxed().collect(Collectors.toSet());
                    first.retainAll(second);
                    first.retainAll(third);
                    Integer left = first.stream().findFirst().orElseThrow();
                    return map.get((char) left.intValue());
                })
                .sum();
//                .collect(Collectors.toList());
        System.out.println(sum);
    }

    private static int calculateLine(String line) {

        String left = line.substring(0, line.length() / 2);
        String right = line.substring(line.length() / 2);
        Set<Integer> leftSet = left.chars().boxed().collect(Collectors.toSet());
        Set<Integer> rightSet = right.chars().boxed().collect(Collectors.toSet());
        leftSet.retainAll(rightSet);
        Integer result = leftSet.stream().findFirst().orElseThrow();
        return map.get((char) result.intValue());

    }

}
