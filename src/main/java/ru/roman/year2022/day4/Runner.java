package ru.roman.year2022.day4;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import ru.roman.common.utils.FileReader;

public class Runner {

    public static void main(String[] args) {
//        List<String> lines = FileReader.readFile("2022/task4.txt");
        Predicate<Section> firstBehaviour = (Section section) -> {
            if (section.firstLeft() <= section.secondLeft() && section.firstRight() >= section.secondRight()) {
                return true;
            } else
                return section.secondLeft() <= section.firstLeft() && section.secondRight() >= section.firstRight();
        };
        Predicate<Section> secondBehaviour = (Section section) -> {
            if (section.firstLeft() <= section.secondLeft() && section.firstRight() >= section.secondLeft()) {
                return true;
            }
            if (section.secondLeft() <= section.firstLeft() && section.secondRight() >= section.firstLeft()) {
                return true;
            }
            return false;
        };
        PairsParser parser = new PairsParser();
        List<Section> sections = parser.parseFile("2022/task4.txt");
        int size = sections.stream()
                .filter(firstBehaviour::test)
                .toList()
                .size();
        System.out.println(size);
        int size2 = sections.stream()
                .filter(secondBehaviour::test)
                .toList()
                .size();
        System.out.println(size2);
    }



}
