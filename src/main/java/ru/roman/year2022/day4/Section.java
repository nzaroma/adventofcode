package ru.roman.year2022.day4;

public record Section(int firstLeft, int firstRight, int secondLeft, int secondRight) {

}
