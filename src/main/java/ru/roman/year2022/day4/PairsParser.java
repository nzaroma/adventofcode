package ru.roman.year2022.day4;

import ru.roman.common.utils.CommonParser;

public class PairsParser implements CommonParser<Section> {

    @Override
    public String getPattern() {

        return "(?<firstLeft>\\d*)-(?<firstRight>\\d*),(?<secondLeft>\\d*)-(?<secondRight>\\d*)";
    }

    @Override
    public Class<Section> getClazz() {

        return Section.class;
    }
}
