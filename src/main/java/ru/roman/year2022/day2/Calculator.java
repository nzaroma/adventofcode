package ru.roman.year2022.day2;

public class Calculator {

    public int calculateRow(GameTest round) {

        int sum = 0;
        sum += round.player().getClassicFigure().getValue();
        if (round.opponent().getClassicFigure() == round.player().getClassicFigure()) {
            sum += 3;
        } else if (round.player().getWinFigure() == round.opponent().getClassicFigure()) {
            sum += 6;
        }
        return sum;
    }

    /**
     * x - 0, y = 3, z = 6
     * @param round
     * @return
     */
    public int calculateSecondRow(GameTest round) {
        int sum = 0;
        switch (round.player()) {
            case X -> {
                sum += 0;
                sum += round.opponent().getWinFigure().getValue();
            }
            case Y -> {
                sum += 3;
                sum += round.opponent().getClassicFigure().getValue();
            }
            case Z -> {
                sum += 6;
                sum += round.opponent().getLoseFigure().getValue();
            }
        }
        return sum;
    }

}
