package ru.roman.year2022.day2;

public record GameTest(Figure opponent, Figure player) {

}
