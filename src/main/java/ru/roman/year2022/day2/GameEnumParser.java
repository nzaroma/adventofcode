package ru.roman.year2022.day2;

import ru.roman.common.utils.CommonParser;

public class GameEnumParser implements CommonParser<GameTest> {

    @Override
    public String getPattern() {

        return "(?<opponent>\\w) (?<player>\\w)";
    }

    @Override
    public Class<GameTest> getClazz() {

        return GameTest.class;
    }
}
