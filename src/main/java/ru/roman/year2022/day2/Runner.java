package ru.roman.year2022.day2;

import java.util.Arrays;
import java.util.List;

import ru.roman.common.utils.CommonParser;
import ru.roman.common.utils.FileReader;

public class Runner {

    public static void main(String[] args) {

//        CommonParser<Game> parser = new GameParser();
//        List<Game> games = parser.parseFile("2022/task2.txt");
//        System.out.println(games);
        GameEnumParser gameEnumParser = new GameEnumParser();
        Calculator calculator = new Calculator();
        List<GameTest> gameTests = gameEnumParser.parseFile("2022/task2.txt");
        System.out.println(gameTests);
        int sum = gameTests.stream().parallel()
                .mapToInt(calculator::calculateRow)
                .sum();
        System.out.println(sum);
/**
 * A Y
 * B X
 * C Z
 */
//        gameTests = Arrays.asList(new GameTest(Figure.A, Figure.Y), new GameTest(Figure.B, Figure.X), new GameTest(Figure.C, Figure.Z));
        int sum1 = gameTests.stream().parallel()
                .mapToInt(calculator::calculateSecondRow)
                .sum();
        System.out.println(sum1);

    }

}
