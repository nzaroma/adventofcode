package ru.roman.year2022.day2;

import ru.roman.common.utils.CommonParser;

public class GameParser implements CommonParser<Game> {

    @Override
    public String getPattern() {

        return "(?<opponent>\\w) (?<player>\\w)";
    }

    @Override
    public Class<Game> getClazz() {

        return Game.class;
    }
}
