package ru.roman.year2022.day2;

public enum Figure {
    A(ClassicFigure.ROCK, ClassicFigure.SCISSORS, ClassicFigure.PAPER),
    B(ClassicFigure.PAPER, ClassicFigure.ROCK, ClassicFigure.SCISSORS),
    C(ClassicFigure.SCISSORS, ClassicFigure.PAPER, ClassicFigure.ROCK),
    X(ClassicFigure.ROCK, ClassicFigure.SCISSORS, ClassicFigure.PAPER),
    Y(ClassicFigure.PAPER, ClassicFigure.ROCK, ClassicFigure.SCISSORS),
    Z(ClassicFigure.SCISSORS, ClassicFigure.PAPER, ClassicFigure.ROCK);

    public enum ClassicFigure {
        ROCK(1), PAPER(2), SCISSORS(3);
        private int value;

        ClassicFigure(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }


    private final ClassicFigure classicFigure;
    private final ClassicFigure winFigure;

    private final ClassicFigure loseFigure;

    Figure(ClassicFigure classicFigure, ClassicFigure winFigure, ClassicFigure loseFigure) {

        this.classicFigure = classicFigure;

        this.winFigure = winFigure;
        this.loseFigure = loseFigure;
    }



    public ClassicFigure getClassicFigure() {

        return classicFigure;
    }

    public ClassicFigure getWinFigure() {

        return winFigure;
    }

    public ClassicFigure getLoseFigure() {
        return loseFigure;
    }
}


