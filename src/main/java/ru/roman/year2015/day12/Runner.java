package ru.roman.year2015.day12;

import ru.roman.common.utils.FileReader;

import java.util.List;

public class Runner {
    public static void main(String[] args) {
        List<String> strings = FileReader.readFile("task12.txt");
        Parser parser = new Parser();
        List<Integer> integers = strings.stream()
                .flatMap(p -> parser.parse(p).stream())
                .toList();
        System.out.println(integers);
        int sum = integers.stream().mapToInt(d -> d).sum();
        System.out.println(sum);


        ParserJson parserJson = new ParserJson();
        String newJson = parserJson.parseJson(strings.get(0));
        int sum1 = parser.parse(newJson).stream().mapToInt(d -> d).sum();
        System.out.println(sum1);

    }
}
