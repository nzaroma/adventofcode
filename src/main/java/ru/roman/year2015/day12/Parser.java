package ru.roman.year2015.day12;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
    public List<Integer> parse(String input) {
        String pattern = "-?\\d+";
        Matcher matcher = Pattern.compile(pattern)
                .matcher(input);
        List<Integer> numbers = new ArrayList<>();
        while (matcher.find()) {
            String substring = input.substring(matcher.start(), matcher.end());
            Integer number = Integer.valueOf(substring);
            numbers.add(number);
        }
        return numbers;
    }

}
