package ru.roman.year2015.day12;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.lang.reflect.Type;
import java.util.*;

public class ParserJson {

    public String parseJson(String input) {
        Gson gson = new Gson();
        Type type = new TypeToken<Map<String, Object>>() {
        }.getType();
        Map<String, Object> map = gson.fromJson(input, type);
        System.out.println("");
        Map<String, Object> newMap = new HashMap<>();
        iterate(map, newMap);
        System.out.println(newMap);
        String s = gson.toJson(newMap);
        System.out.println(s);
        return s;
    }

    private boolean containsRedObjectEntry(Map.Entry<String, Object> entry) {
        Object value = entry.getValue();
        if (value instanceof String s) {
            return "red".equalsIgnoreCase(s);
        }
        return false;
    }


    private void iterate(Object inputObject, Object outputObject) {
        if (inputObject instanceof Map inputMap) {
            Map<String, Object> input = inputMap;
            Map<String, Object> output = (Map<String, Object>) outputObject;
            boolean hasRed = input.entrySet().stream().anyMatch(this::containsRedObjectEntry);
            if (hasRed) {
                return;
            }
            input.entrySet().forEach(entry -> {
                Object value = entry.getValue();
                if (value instanceof Map map) {
                    HashMap<String, Object> innerMap = new HashMap<>();
                    output.put(entry.getKey(), innerMap);
                    iterate(map, innerMap);
                } else if (value instanceof List list) {
                    ArrayList<Object> innerList = new ArrayList<>();
                    output.put(entry.getKey(), innerList);
                    iterate(list, innerList);
                } else {
                    output.put(entry.getKey(), value);
                }

            });
        } else {
            List<Object> inputList = (List<Object>) inputObject;
            List<Object> output = (List<Object>) outputObject;
            inputList.forEach(value -> {
                if (value instanceof Map map) {
                    HashMap<String, Object> innerMap = new HashMap<>();
                    output.add(innerMap);
                    iterate(map, innerMap);
                } else if (value instanceof List list) {
                    ArrayList<Object> innerList = new ArrayList<>();
                    output.add(innerList);
                    iterate(list, innerList);
                } else {
                    output.add(value);
                }
            });
        }
    }
}



