package ru.roman.year2015.day18;

public class Calculator {
    private int[][] input;

    public Calculator(int[][] initial) {
        this.input = initial;
    }

    public int calculateLights(int steps, boolean isNew) {
        for (int i = 0; i < steps; i++) {
            if (isNew) {
                input[0][0] = 1;
                input[0][input[0].length - 1] = 1;
                input[input[0].length - 1][0] = 1;
                input[input[0].length - 1][input.length - 1] = 1;
            }
            input = calculateStep();
        }
        if (isNew) {
            input[0][0] = 1;
            input[0][input[0].length - 1] = 1;
            input[input[0].length - 1][0] = 1;
            input[input[0].length - 1][input.length - 1] = 1;
        }
        int num = 0;
        for (int i = 0; i < input[0].length; i++) {
            for (int j = 0; j < input.length; j++) {
                num += input[i][j];
            }
        }
        return num;
    }

    private int[][] calculateStep() {
        int[][] result = new int[input[0].length][input.length];
        for (int i = 0; i < input[0].length; i++) {
            for (int j = 0; j < input.length; j++) {
                result[i][j] = calculateCell(i, j);
            }
        }
        return result;
    }

    private int calculateCell(int i, int j) {
        int stateBefore = input[i][j];

        int totalOn = 0;
        for (int k = -1; k <= 1; k++) {
            for (int l = -1; l <= 1; l++) {
                if (i + k < 0 || j + l < 0 || i+k == input[0].length || j+l == input.length || (k == 0 && l == 0)) {
                    continue;
                }
                int cellValue = input[i + k][j + l];
                totalOn += cellValue;
            }
        }
        if (stateBefore == 1 && (totalOn ==2 || totalOn==3) ) {
            return 1;
        }
        if (stateBefore == 0 && totalOn == 3) {
            return 1;
        }
        return 0;
    }
}
