package ru.roman.year2015.day18;

import java.util.List;

public class Parser {

    public int[][] parseFile(List<String> lines) {
        int[][] result = new int[lines.size()][lines.size()];
        for (int i = 0; i < lines.size(); i++) {
            result[i] =parse(lines.get(i));
        }
        return result;
    }

    public int[] parse(String line) {
        int[] arrayLine = new int[line.length()];
        return line.chars()
                .map(d -> parseChar(d))
                .toArray();
    }

    private int parseChar(int c) {
        return switch (c) {
            case '#' -> 1;
            case '.' -> 0;
            default -> throw new IllegalStateException("Unexpected value: " + c);
        };
    }
}
