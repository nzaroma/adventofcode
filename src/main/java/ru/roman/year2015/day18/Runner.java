package ru.roman.year2015.day18;

import ru.roman.common.utils.FileReader;

import java.util.List;

public class Runner {
    public static void main(String[] args) {
        Parser parser = new Parser();
        List<String> strings = FileReader.readFile("task18.txt");
        int[][] ints = parser.parseFile(strings);
        Calculator calculator = new Calculator(ints);
//        int result = calculator.calculateLights(100, false);
//        System.out.println(result);
        int result = calculator.calculateLights(100, true);
        System.out.println(result);
    }
}
