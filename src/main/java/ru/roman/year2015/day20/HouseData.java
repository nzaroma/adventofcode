package ru.roman.year2015.day20;

import java.util.concurrent.atomic.AtomicInteger;

public class HouseData {
    private volatile boolean stop;
    private AtomicInteger currentHouse = new AtomicInteger();
    private AtomicInteger resultSum = new AtomicInteger();

    public boolean isStop() {
        return stop;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public AtomicInteger getCurrentHouse() {
        return currentHouse;
    }

    public void setCurrentHouse(AtomicInteger currentHouse) {
        this.currentHouse = currentHouse;
    }

    public void setResult(int result) {
        resultSum.compareAndSet(0, result);
    }

    public AtomicInteger getResultSum() {
        return resultSum;
    }

    public void setResultSum(AtomicInteger resultSum) {
        this.resultSum = resultSum;
    }
}
