package ru.roman.year2015.day20;

public class HouseCalculator implements Runnable {

    private HouseData houseData;
    int limit = 34000000;

    public HouseCalculator(HouseData houseData) {
        this.houseData = houseData;
    }

    @Override
    public void run() {
        while (!houseData.isStop()) {
            int houseSum = 0;
            int currentHouse = houseData.getCurrentHouse().getAndIncrement();
            for (int i = 1; i <= currentHouse; i++) {
                if (currentHouse % i == 0) {
                    int timesVisited = currentHouse / i;
                    if (timesVisited <= 50) {
                        houseSum+= i * 11;
                    }
                }
            }
            if (houseSum >= limit) {
                houseData.setStop(true);
                System.out.println("house sum for " + currentHouse + " is " + houseSum);
                houseData.setResult(currentHouse);
                System.out.println("result: " + houseData.getResultSum().get());
            }
        }
    }
}
