package ru.roman.year2015.day20;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Calculator {
    private List<Integer> simpleNumbers = new ArrayList<>();

    int limit = 34000000;


    public static void main(String[] args) {
//        new Calculator().getHouse();
        HouseData houseData = new HouseData();
        IntStream.rangeClosed(0, 8)
                .parallel()
                .mapToObj(value -> new Thread(new HouseCalculator(houseData)))
                .peek(thread -> thread.start())
                .forEach(thread -> {
                    try {
                        thread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
    }

    public int getHouse() {
        boolean stop = false;
        int currentHouse = 1;
        while (!stop) {
//            if (currentHouse == 10) {
//                break;
//            }
            int houseSum = 0;
            for (int i = 1; i <= currentHouse; i++) {
//                System.out.println("checking house " + i);
                if ((currentHouse - i) < 50 && currentHouse % i == 0) {
                    houseSum+= i * 10;
                }
            }
//            System.out.println("house sum for " + currentHouse + " is " + houseSum);
            if (houseSum >= limit) {
                System.out.println("house sum for " + currentHouse + " is " + houseSum);
                break;
            }
            currentHouse++;
        }
        return 0;
    }

    class HouseCalculator2 implements Runnable {

        private HouseData houseData;

        public HouseCalculator2(HouseData houseData) {
            this.houseData = houseData;
        }

        @Override
        public void run() {
            while (!houseData.isStop()) {
                int houseSum = 0;
                int currentHouse = houseData.getCurrentHouse().getAndIncrement();
                for (int i = 1; i <= currentHouse; i++) {
                    if (currentHouse % i == 0) {
                        houseSum+= i * 10;
                    }
                }
                if (houseSum >= limit) {
                    System.out.println("house sum for " + currentHouse + " is " + houseSum);
                    int result = houseData.getResultSum().addAndGet(houseSum);
                    System.out.println("result: " + result);
                    houseData.setStop(true);
                    break;
                }
            }
        }
    }
}

/*

500
2: 250
3: 498+2

 */
