package ru.roman.year2015.day19;

import java.util.ArrayList;
import java.util.List;

public class Calculator {
    public int getCombinationCount(String input, List<Transition> transitions) {
        List<String> fromTransitions = transitions.stream()
                .map(Transition::from)
                .toList();
        List<String> results = new ArrayList<>();
        for (int i = 0; i < input.length(); i++) {
            String first = input.substring(0, i);
            String second = input.substring(i, i + 1);
            String third = input.substring(i + 1);

            if (isLegalFrom(second, fromTransitions)) {
                List<String> substitutions = getTransitions(second, transitions);
                for (String substitution : substitutions) {
                    String result = first + substitution + third;
                    results.add(result);
                }
            }
            if (i + 1 <= input.length() - 1) {
                second = input.substring(i, i + 2);
                third = input.substring(i + 2);
                if (isLegalFrom(second, fromTransitions)) {
                    List<String> substitutions = getTransitions(second, transitions);
                    for (String substitution : substitutions) {
                        String result = first + substitution + third;
                        results.add(result);
                    }
                }
            }
        }
        return results.stream().distinct().toList().size();
    }

    private List<String> getTransitions(String from, List<Transition> transitions) {
        return transitions.stream()
                .filter(d -> d.from().equals(from))
                .map(Transition::to)
                .toList();
    }

    private boolean isLegalFrom(String from, List<String> fromTransitions) {
        return fromTransitions.contains(from);
    }

//    public int calculateSteps(String input, List<Transition> transitions) {
//        boolean stop = false;
//        while (!stop) {
//
//        }
//    }
}
