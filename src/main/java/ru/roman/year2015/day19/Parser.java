package ru.roman.year2015.day19;

import ru.roman.common.utils.CommonParser;

public class Parser implements CommonParser<Transition> {
    @Override
    public String getPattern() {
        return "(?<from>\\w*) => (?<to>\\w*)";
    }

    @Override
    public Class<Transition> getClazz() {
        return Transition.class;
    }
}
