package ru.roman.year2015.day19;

public record Transition(String from, String to) {
}
