package ru.roman.year2015.day8;

import ru.roman.common.utils.FileReader;

import java.util.List;

public class Matchsticks {
    public static void main(String[] args) {

        List<String> strings = FileReader.readFile("task8.txt");
        Parser parser = new Parser();
        List<Result> results = parser.parseLines(strings);
        long total = results.stream()
                .mapToInt(Result::total)
                .sum();
        int real = results.stream()
                .mapToInt(Result::real)
                .sum();
        int encoded = results.stream()
                .mapToInt(Result::encoded)
                .sum();
        System.out.println(total - real);
        System.out.println(encoded - total);

    }
}
