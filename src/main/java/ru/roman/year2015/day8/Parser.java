package ru.roman.year2015.day8;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

    public List<Result> parseLines(List<String> input) {
        return input.stream()
                .map(this::parseLine)
                .toList();
    }

    public Result parseLine(String input) {
        String hexademical = "\\\\\"|\\\\\\\\|\\\\x[A-Fa-f0-9]{2}";
        String quotesOpeningClosing = "^\".*\"$";

        int encodedLength = input.length();
        int realLength = input.length();
        if (Pattern.compile(quotesOpeningClosing).matcher(input).matches()) {
            realLength-=2;
            encodedLength+=4;
        }

        Matcher matcher = Pattern.compile(hexademical)
                .matcher(input);
        while (matcher.find()) {
            if (matcher.end() - matcher.start() == 4) {
                realLength-=3;
                encodedLength+=1;
            } else if (matcher.end() - matcher.start() == 2) {
                realLength-=1;
                encodedLength+=2;
            }
        }
        return new Result(input.length(), realLength, encodedLength);
    }
}
