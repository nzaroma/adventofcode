package ru.roman.year2015.day8;

public record Result(int total, int real, int encoded) {
}
