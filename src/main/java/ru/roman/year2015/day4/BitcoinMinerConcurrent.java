package ru.roman.year2015.day4;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class BitcoinMinerConcurrent {
    public static void main(String[] args) throws NoSuchAlgorithmException, InterruptedException {
        String input = "yzbqklnj";
        int abcdef = new BitcoinMinerConcurrent().getMinimumNumber(input, 5);
        System.out.println(abcdef);
        long startTime = System.currentTimeMillis();
        abcdef = new BitcoinMinerConcurrent().getMinimumNumber(input, 6);
        System.out.println(abcdef);
        long endTime = System.currentTimeMillis();
        System.out.println("Time: " + (endTime - startTime));

        startTime = System.currentTimeMillis();
        Counter counter = new Counter();

        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            Thread thread = new Thread(new MinerConcurrent(counter, input, 6));
            threads.add(thread);
            thread.start();
        }
        for (Thread thread : threads) {
            thread.join();
        }
        System.out.println("Result total: " + counter.getResult().get());
        endTime = System.currentTimeMillis();
        System.out.println("Time: " + (endTime - startTime));

//        startTime = System.currentTimeMillis();
//        MinerConcurrent minerConcurrent1 = new MinerConcurrent(new Counter(), input, 7);
//        IntStream.rangeClosed(0, 6)
//                .parallel()
//                .mapToObj(value -> new Thread(minerConcurrent1))
//                .peek(Thread::start)
//                .forEach(thread -> {
//                    try {
//                        thread.join();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                });
//        endTime = System.currentTimeMillis();
//        System.out.println("Time: " + (endTime - startTime));
//        thread3.start();
//        thread4.start();
//        thread5.start();
//        thread6.start();

    }

    private record HashValue(String input, int value, String hash) {
        public boolean hasLeadingZeroes(int zeroCount) {
            return hash.startsWith("0".repeat(zeroCount));
        }
    }

    public int getMinimumNumber(String input, int zeroCount) throws NoSuchAlgorithmException {
        final MessageDigest md5 = MessageDigest.getInstance("MD5");

        return IntStream.iterate(0, operand -> operand + 1)
                .mapToObj(value -> createHash(md5, input, value))
                .filter(hashValue -> hashValue.hasLeadingZeroes(zeroCount))
                .mapToInt(HashValue::value)
                .findFirst()
                .orElse(-1);

    }

    private HashValue createHash(MessageDigest md5, String input, int i) {
        final String modifiedInput = input + i;
        md5.update(modifiedInput.getBytes());
        final byte[] byteHash = md5.digest();
        final String result = DatatypeConverter.printHexBinary(byteHash);
        return new HashValue(input, i, result);
    }





}
