package ru.roman.year2015.day4;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.stream.IntStream;

public class BitcoinMiner {

    private record HashValue(String input, int value, String hash) {
        public boolean hasLeadingZeroes(int zeroCount) {
            return hash.startsWith("0".repeat(zeroCount));
        }
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        int abcdef = new BitcoinMiner().getMinimumNuber("yzbqklnj", 5);
        System.out.println(abcdef);
        abcdef = new BitcoinMiner().getMinimumNuber("yzbqklnj", 6);
        System.out.println(abcdef);
    }

    public int getMinimumNuber(String input, int zeroCount) throws NoSuchAlgorithmException {
        final MessageDigest md5 = MessageDigest.getInstance("MD5");

        return IntStream.iterate(0, operand -> operand + 1)
                .mapToObj(value -> createHash(md5, input, value))
                .filter(hashValue -> hashValue.hasLeadingZeroes(zeroCount))
                .mapToInt(HashValue::value)
                .findFirst()
                .orElse(-1);

    }

    private HashValue createHash(MessageDigest md5, String input, int i) {
        final String modifiedInput = input + i;
        md5.update(modifiedInput.getBytes());
        final byte[] byteHash = md5.digest();
        final String result = DatatypeConverter.printHexBinary(byteHash);
        return new HashValue(input, i, result);
    }


}
