package ru.roman.year2015.day4;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MinerConcurrent implements Runnable {

    private record HashValue(String input, int value, String hash) {
        public boolean hasLeadingZeroes(int zeroCount) {
            return hash.startsWith("0".repeat(zeroCount));
        }
    }

//    private final ThreadLocal<MessageDigest> md5 = new ThreadLocal<>();
    private MessageDigest md5;
    private final Counter counter;
    private final String input;
    private final int leadingZeros;

    public MinerConcurrent(Counter counter, String input, int leadingZeros) throws NoSuchAlgorithmException {
        this.counter = counter;


        md5 = MessageDigest.getInstance("MD5");

        this.input = input;
        this.leadingZeros = leadingZeros;
    }

    @Override
    public void run() {
//        try {
//            md5.set(MessageDigest.getInstance("MD5"));
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
        while (!counter.isStop()) {
            int localIndex = counter.getCounter().incrementAndGet();
            HashValue hash = createHash(localIndex);
            if (hash.hasLeadingZeroes(leadingZeros)) {
                counter.setStop(true);
                counter.getResult().set(localIndex);
                System.out.println("Result: " + localIndex);
            }
        }
    }

    private HashValue createHash(int i) {
        final String modifiedInput = input + i;
//        md5.get().update(modifiedInput.getBytes());
        md5.update(modifiedInput.getBytes());
//        final byte[] byteHash = md5.get().digest();
        final byte[] byteHash = md5.digest();
        final String result = DatatypeConverter.printHexBinary(byteHash);
        return new HashValue(input, i, result);
    }
}
