package ru.roman.year2015.day4;

import java.util.concurrent.atomic.AtomicInteger;

public class Counter {
    private AtomicInteger counter = new AtomicInteger();
    private volatile boolean stop;
    private AtomicInteger result = new AtomicInteger();

    public AtomicInteger getResult() {
        return result;
    }

    public void setResult(AtomicInteger result) {
        this.result = result;
    }

    public AtomicInteger getCounter() {
        return counter;
    }

    public boolean isStop() {
        return stop;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

}
