package ru.roman.year2015.day17;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class Combinator {
    private int limit;
    private List<Integer> inputList;
    private List<List<Integer>> combinations;

    public Combinator(int limit, List<Integer> inputList) {
        this.limit = limit;
        this.inputList = inputList;
        this.combinations = new ArrayList<>();
    }

    public int combine3() {
        int listSize = inputList.size();
        long upTo = (long) Math.pow(2, listSize) - 1;
        System.out.println(upTo);
        String binaryLong = Long.toBinaryString(upTo);
        System.out.println(binaryLong);
        int result = LongStream.rangeClosed(0, upTo)
                .mapToObj(Long::toBinaryString)
                .map(r -> {
                    int length = r.length();
                    if (length < listSize) {
                        int toAdd = listSize - length;
                        return "0".repeat(toAdd) + r;
                    }
                    return r;
                })
                .map(representation -> {
//                    System.out.println("r="+representation);
                    int sum = 0;
                    for (int i = 0; i < representation.length(); i++) {
                        int c = parseChar(representation.charAt(i));
                        int localSum = inputList.get(i) * c;
                        sum += localSum;
                    }
//                    System.out.println("sum="+sum);
                    return sum;
                })
                .filter(d -> d == limit)
                .toList().size();
        return result;
    }

    public int combineMinContainers() {
        Map<Integer, Integer> map = new HashMap<>();
        int listSize = inputList.size();
        long upTo = (long) Math.pow(2, listSize) - 1;
        System.out.println(upTo);
        String binaryLong = Long.toBinaryString(upTo);
        System.out.println(binaryLong);
        int result = LongStream.rangeClosed(0, upTo)
                .mapToObj(Long::toBinaryString)
                .map(r -> {
                    int length = r.length();
                    if (length < listSize) {
                        int toAdd = listSize - length;
                        return "0".repeat(toAdd) + r;
                    }
                    return r;
                })
                .filter(representation -> {
//                    System.out.println("r="+representation);
                    int sum = 0;
                    int num = 0;
                    for (int i = 0; i < representation.length(); i++) {
                        int c = parseChar(representation.charAt(i));
                        if (c == 1) {
                            num++;
                        }
                        int localSum = inputList.get(i) * c;
                        sum += localSum;
                    }
//                    System.out.println("sum="+sum);
                    if (sum == limit) {
                        map.put(num, map.getOrDefault(num, 0) + 1);
                    }
                    return sum == limit;
                })
                .toList().size();
        System.out.println(map);
        int first = map.keySet().stream().sorted().findFirst().orElseThrow();
        return map.get(first);
    }

    private int parseChar(char c) {
        return switch (c) {
            case '0' -> 0;
            case '1' -> 1;
            default -> throw new IllegalArgumentException();
        };
    }
}
/**
 20 15 10 5 5
 20,15 15,15 10,15 5,15 5,15




 */