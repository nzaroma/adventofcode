package ru.roman.year2015.day17;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class Runner {
    public static void main(String[] args) {
        int input = 150;
        List<Integer> containers = Stream.of(11, 30, 47, 31, 32, 36, 3, 1, 5, 3, 32, 36, 15, 11, 46, 26, 28, 1, 19, 3).sorted(Comparator.reverseOrder()).toList();
        Combinator combinator = new Combinator(input, containers);
        int i = combinator.combineMinContainers();
        System.out.println(i);
    }




}
