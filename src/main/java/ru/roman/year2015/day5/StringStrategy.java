package ru.roman.year2015.day5;

import java.util.List;
import java.util.function.Predicate;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public interface StringStrategy {

    List<String> STOP_WORDS = List.of("ab", "cd", "pq", "xy");

    List<Predicate<String>> getPredicates();

    default long apply(List<String> strings) {
        return strings.stream()
                .filter(getPredicates().stream().reduce(p -> true, Predicate::and))
                .count();
    }

    default Stream<String> getPatternMatcherResults(String pattern, String line) {
        return Pattern.compile(pattern)
                .matcher(line)
                .results()
                .map(MatchResult::group);
    }

    default boolean containsVowels(String line) {
        long sum = getPatternMatcherResults("[aeiou]", line)
                .count();
        return sum >= 3;
    }

    default boolean containsRepeatedSymbols(String line) {
        return getPatternMatcherResults("(.)\\1+", line)
                .findAny()
                .isPresent();
    }

    default boolean doesNotContainProhibited(String line) {
        return STOP_WORDS.stream().noneMatch(line::contains);
    }

    default boolean containsPairs(String line) {
        return getPatternMatcherResults("(..)(\\w)*\\1", line)
                .findAny()
                .isPresent();
    }

    default boolean containsSameSymbolWithSeparator(String line) {
        return getPatternMatcherResults("(.)(\\w){1}\\1", line)
                .findAny()
                .isPresent();
    }


}

class EasyStrategy implements StringStrategy {

    @Override
    public List<Predicate<String>> getPredicates() {
        return List.of(
                StringStrategy.super::containsVowels,
                StringStrategy.super::containsRepeatedSymbols,
                StringStrategy.super::doesNotContainProhibited);
    }


}

class AdvancedStrategy implements StringStrategy {
    @Override
    public List<Predicate<String>> getPredicates() {
        return List.of(
                StringStrategy.super::containsPairs,
                StringStrategy.super::containsSameSymbolWithSeparator
        );
    }
}