package ru.roman.year2015.day5;

import ru.roman.common.utils.FileReader;

import java.util.List;

public class NiceString {


    public static void main(String[] args) {
        List<String> strings = FileReader.readFile("task5.txt");
        System.out.println(new NiceString().count(strings, new EasyStrategy()));
        System.out.println(new NiceString().count(strings, new AdvancedStrategy()));

    }

    public long count(List<String> strings, StringStrategy stringStrategy) {
        return stringStrategy.apply(strings);
    }

}
