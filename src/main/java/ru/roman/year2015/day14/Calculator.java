package ru.roman.year2015.day14;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Calculator {
    public int getWinnerDistance(List<DeerSpec> deerSpecs, int time) {
        return deerSpecs.stream()
                .mapToInt(deer -> calculateDeer(deer, time))
                .max()
                .orElseThrow();
    }

    public int getMaxPointDeer(List<DeerSpec> deerSpecs, int time) {
        Map<Integer, List<DeerSpec>> secondToDeer = new HashMap<>();
        IntStream.range(1, time)
                .forEach(second -> {
                    List<DeerDistance> deerDistances = deerSpecs.stream()
                            .map(deer -> new DeerDistance(deer, calculateDeer(deer, second)))
                            .sorted((o1, o2) -> Integer.compare(o2.distance(), o1.distance()))
                            .toList();
                    int maxDistance = deerDistances.get(0).distance();
                    List<DeerSpec> maxDeerDistances = deerDistances.stream()
                            .filter(deerDistance -> deerDistance.distance() == maxDistance)
                            .map(DeerDistance::deerSpec)
                            .toList();
                    secondToDeer.put(second, maxDeerDistances);
                });

        return secondToDeer.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(Function.identity(), deerSpec -> 1, Integer::sum))
                .values()
                .stream().max(Comparator.naturalOrder())
                .orElseThrow();

    }

    private int calculateDeer(DeerSpec deerSpec, int time) {
        int fly = deerSpec.flyDuration();
        int rest = deerSpec.restDuration();
        int fullCycles = time / (fly + rest);
        int remainder = time % (fly + rest);
        return calculateCycle(deerSpec) * fullCycles + calculateRemainder(deerSpec, remainder);
    }

    private int calculateCycle(DeerSpec deerSpec) {
        return deerSpec.speed() * deerSpec.flyDuration();
    }

    private int calculateRemainder(DeerSpec deerSpec, int remainder) {
        if (remainder <= deerSpec.flyDuration()) {
            return deerSpec.speed() * remainder;
        }
        return deerSpec.speed() * deerSpec.flyDuration();
    }
}
