package ru.roman.year2015.day14;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
    public DeerSpec parse(String line) {
        Matcher matcher = Pattern.compile("(\\w*) can fly (\\d*) km\\/s for (\\d*) seconds, but then must rest for (\\d*) seconds.").matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("no matches");
        }
        String name = matcher.group(1);
        int speed = Integer.parseInt(matcher.group(2));
        int flyDuration = Integer.parseInt(matcher.group(3));
        int restDuration = Integer.parseInt(matcher.group(4));
        return new DeerSpec(name, speed, flyDuration, restDuration);
    }
}
