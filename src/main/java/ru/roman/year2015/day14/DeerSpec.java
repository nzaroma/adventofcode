package ru.roman.year2015.day14;

public record DeerSpec(String name, int speed, int flyDuration, int restDuration) {
}
