package ru.roman.year2015.day14;

public record DeerDistance(DeerSpec deerSpec, int distance) {
}
