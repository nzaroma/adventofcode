package ru.roman.year2015.day14;

import ru.roman.common.utils.FileReader;

import java.util.List;

public class Runner {
    public static void main(String[] args) {
        List<String> strings = FileReader.readFile("task14.txt");
        Parser parser = new Parser();
        List<DeerSpec> deerSpecs = strings.stream()
                .map(parser::parse)
                .toList();
        Calculator calculator = new Calculator();
        int winnerDistance = calculator.getWinnerDistance(deerSpecs, 2503);
        System.out.println(winnerDistance);

        int maxPointDeer = calculator.getMaxPointDeer(deerSpecs, 2503);
        System.out.println(maxPointDeer);
    }
}
