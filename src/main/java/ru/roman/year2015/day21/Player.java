package ru.roman.year2015.day21;

import java.util.List;

public class Player {

    public Player(List<Item> inventory) {
        this.inventory = inventory;
        cost = inventory.stream().mapToInt(Item::cost).sum();
        str = inventory.stream().mapToInt(Item::damage).sum();
        def = inventory.stream().mapToInt(Item::armor).sum();
        hp = 100;
    }

    public Player(int hp, int str, int def) {
        this.hp = hp;
        this.str = str;
        this.def = def;
    }

    public List<Item> getInventory() {
        return inventory;
    }

    public int getHp() {
        return hp;
    }

    public int getStr() {
        return str;
    }

    public int getDef() {
        return def;
    }

    public int getCost() {
        return cost;
    }

    public void setInventory(List<Item> inventory) {
        this.inventory = inventory;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    private List<Item> inventory;
    private int hp;
    private int str;
    private int def;
    private int cost;

    @Override
    public String toString() {
        return "Player{" +
                "hp=" + hp +
                ", str=" + str +
                ", def=" + def +
                '}';
    }
}
