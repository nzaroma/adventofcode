package ru.roman.year2015.day21;

public record BattleResult(boolean playerWin, int cost) {
    @Override
    public String toString() {
        return "BattleResult{" +
                "playerWin=" + playerWin +
                ", cost=" + cost +
                '}';
    }
}
