package ru.roman.year2015.day21;

public record Item(String name, int cost, int damage, int armor, ItemType itemType) {
    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", cost=" + cost +
                ", damage=" + damage +
                ", armor=" + armor +
                ", itemType=" + itemType +
                '}';
    }
}
