package ru.roman.year2015.day21;

import java.util.Comparator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.function.Predicate;
import java.util.stream.IntStream;

public class Runner {
    public static void main(String[] args) throws InterruptedException {
        List<Item> weapons = List.of(
                new Item("Dagger", 8, 4, 0, ItemType.WEAPON),
                new Item("Shortsword", 10, 5, 0, ItemType.WEAPON),
                new Item("Warhammer", 25, 6, 0, ItemType.WEAPON),
                new Item("Longsword", 40, 7, 0, ItemType.WEAPON),
                new Item("Greataxe", 74, 8, 0, ItemType.WEAPON)
        );

        List<Item> armors = List.of(
                new Item("Empty", 0, 0, 0, ItemType.ARMOR),
                new Item("Leather", 13, 0, 1, ItemType.ARMOR),
                new Item("Chainmail", 31, 0, 2, ItemType.ARMOR),
                new Item("Splintmail", 53, 0, 3, ItemType.ARMOR),
                new Item("Bandedmail", 75, 0, 4, ItemType.ARMOR),
                new Item("Platemail", 102, 0, 5, ItemType.ARMOR)
        );

        List<Item> rings = List.of(
                new Item("Empty", 0, 0, 0, ItemType.RING),
                new Item("Empty2", 0, 0, 0, ItemType.RING),
                new Item("Damage +1", 25, 1, 0, ItemType.RING),
                new Item("Damage +2", 50, 2, 0, ItemType.RING),
                new Item("Damage +3", 100, 3, 0, ItemType.RING),
                new Item("Defense +1", 20, 0, 1, ItemType.RING),
                new Item("Defense +2", 40, 0, 2, ItemType.RING),
                new Item("Defense +3", 80, 0, 3, ItemType.RING)
        );
        int size = weapons.size() * armors.size() * rings.size() * rings.size() - (weapons.size() * armors.size() * rings.size());
        Queue<List<Item>> itemQueue = new ArrayBlockingQueue<>(size, false);
        for (Item weapon : weapons) {
            for (Item armor : armors) {
                for (Item ring1 : rings) {
                    for (Item ring2 : rings) {
                        if (ring1 == ring2) {
                            continue;
                        }
                        List<Item> localItems = List.of(weapon, armor, ring1, ring2);
                        itemQueue.add(localItems);
                    }
                }
            }
        }


        Queue<BattleResult> resultSet = new ConcurrentLinkedQueue<>();
        System.out.println("Starting threads");
        Player boss = new Player(103, 9, 2);
//        IntStream.rangeClosed(0, 7)
//                .parallel()
//                .mapToObj(value -> new Thread(new ConcurrentBattle(itemQueue, boss, resultSet)))
//                .peek(Thread::start)
//                .forEach(thread -> {
//                    try {
//                        thread.join();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                });


        ExecutorService executorService = Executors.newFixedThreadPool(8);
        IntStream.rangeClosed(0, 7)
                .mapToObj(i -> new ConcurrentBattle(itemQueue, boss, resultSet))
                .forEach(executorService::submit);
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);

        System.out.println("Finishnig threads");
        BattleResult first = resultSet.stream()
                .filter(BattleResult::playerWin)
                .min(Comparator.comparingInt(BattleResult::cost))
                .orElseThrow();
        System.out.println(first);
        BattleResult loseMaxCost = resultSet.stream()
                .filter(Predicate.not(BattleResult::playerWin))
                .max(Comparator.comparingInt(BattleResult::cost))
                .orElseThrow();
        System.out.println(loseMaxCost);
        new Thread().interrupt();
    }
}
