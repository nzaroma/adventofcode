package ru.roman.year2015.day21;

public class Battle {
    public void battle(Player player, Player boss) {
        boolean stop = false;
        int turn = 0;
        while (!stop) {
            System.out.println("Turn " + turn);
            stop = checkPlayerLost(player, "player");
            if (stop) {
                break;
            }
            stop = checkPlayerLost(boss, "boss");
            if (stop) {
                break;
            }
            doTurn(player, boss);
            stop = checkPlayerLost(boss, "boss");
            System.out.println(boss);
            if (stop) {
                break;
            }

            doTurn(boss, player);
            stop = checkPlayerLost(player, "player");
            System.out.println(player);
            if (stop) {
                break;
            }
            turn++;
        }
    }

    private void doTurn(Player first, Player second) {
        int damage = first.getStr();
        int def = second.getDef();
        int intermediateDamage = damage - def;
        int resultDamage = Math.max(intermediateDamage, 1);
        second.setHp(second.getHp() - resultDamage);
    }

    private boolean checkPlayerLost(Player player, String who) {
        if (player.getHp() <= 0) {
            System.out.println(who + " lost");
            return true;
        }
        return false;
    }
}
