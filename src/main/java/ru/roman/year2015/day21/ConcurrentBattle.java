package ru.roman.year2015.day21;

import java.util.List;
import java.util.Queue;

public class ConcurrentBattle implements Runnable {

    private Queue<List<Item>> queue;
    private Player boss;
    private Queue<BattleResult> results;

    public ConcurrentBattle(Queue<List<Item>> queue, Player boss, Queue<BattleResult> results) {
        this.queue = queue;
        this.boss = boss;
        this.results = results;
    }

    private Player copyBoss() {
        return new Player(boss.getHp(), boss.getStr(), boss.getDef());
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+ " starting");
        while (!queue.isEmpty()) {
            final List<Item> items = queue.poll();
            if (items == null) {
                System.out.println("empty queue");
                break;
            }
            final Player newPlayer = createPlayer(items);
            final Player copyBoss = copyBoss();
            final BattleResult battleResult = performBattle(newPlayer, copyBoss);
            results.add(battleResult);
        }
        System.out.println(Thread.currentThread().getName()+ " finishing");
    }

    private BattleResult performBattle(Player player, Player boss) {
        boolean stop = false;
        while (!stop) {
            doTurn(player, boss);
            stop = checkPlayerLost(boss);
            if (stop) {
                return new BattleResult(true, player.getCost());
            }

            doTurn(boss, player);
            stop = checkPlayerLost(player);
            if (stop) {
                return new BattleResult(false, player.getCost());
            }
        }
        throw new IllegalArgumentException("oops");
    }

    private void doTurn(Player first, Player second) {
        int damage = first.getStr();
        int def = second.getDef();
        int intermediateDamage = damage - def;
        int resultDamage = Math.max(intermediateDamage, 1);
        second.setHp(second.getHp() - resultDamage);
    }

    private boolean checkPlayerLost(Player player) {
        return player.getHp() <= 0;
    }

    private Player createPlayer(List<Item> items) {
        return new Player(items);
    }

}
