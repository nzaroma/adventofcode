package ru.roman.year2015.day15;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
    public Ingredient parse(String line) {
        Matcher matcher = Pattern.compile("(\\w*): capacity (-?\\d*), durability (-?\\d*), flavor (-?\\d*), texture (-?\\d*), calories (-?\\d*)").matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("no matches");
        }
        String name = matcher.group(1);
        int capacity = Integer.parseInt(matcher.group(2));
        int durability = Integer.parseInt(matcher.group(3));
        int flavor = Integer.parseInt(matcher.group(4));
        int texture = Integer.parseInt(matcher.group(5));
        int calories = Integer.parseInt(matcher.group(6));
        return new Ingredient(name, capacity, durability, flavor, texture, calories);
    }
}
