package ru.roman.year2015.day15;

import ru.roman.common.utils.FileReader;

import java.util.List;

public class Runner {
    public static void main(String[] args) {
        List<String> strings = FileReader.readFile("task15.txt");
        Parser parser = new Parser();
        List<Ingredient> ingredients = strings.stream()
                .map(parser::parse)
                .toList();
        CalculatingStrategy strategy = new WithoutCaloriesCalculatingStrategy(ingredients);
        int apply = strategy.apply();
        System.out.println(apply);

        CalculatingStrategy strategy2 = new CaloriesCalculatingStrategy(ingredients);
        int apply1 = strategy2.apply();
        System.out.println(apply1);
    }
}
