package ru.roman.year2015.day15;

import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

public class WithoutCaloriesCalculatingStrategy implements CalculatingStrategy {

    private final Incrementor incrementor;
    private List<Ingredient> ingredients;

    public WithoutCaloriesCalculatingStrategy(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
        this.incrementor = new Incrementor(ingredients.size());
    }

    @Override
    public int apply() {
        int maxSum = Integer.MIN_VALUE;
        while (incrementor.hasNext()) {
            List<Integer> allocations = incrementor.next();
            int sum = calculateSum(ingredients, allocations);
            if (sum > maxSum) {
                maxSum = sum;
            }
        }
        return maxSum;
    }




}
/**
 * 1 - 100
 * 2 - 100,0
 * 2 - 99, 1
 * 3
 * 100,0,0
 * <p>
 * 0,0,100
 * 0,1,99
 * 0,100,0
 * 1,0,99
 * 1,1,98
 * 1,99,0
 * <p>
 * 100,0.0
 */