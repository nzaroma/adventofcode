package ru.roman.year2015.day15;

import java.util.List;

public class CaloriesCalculatingStrategy implements CalculatingStrategy{

    private final Incrementor incrementor;
    private List<Ingredient> ingredients;

    public CaloriesCalculatingStrategy(List<Ingredient> ingredients) {
        this.incrementor = new Incrementor(ingredients.size());
        this.ingredients = ingredients;
    }

    @Override
    public int apply() {
        int maxSum = Integer.MIN_VALUE;
        while (incrementor.hasNext()) {
            List<Integer> allocations = incrementor.next();
            int calories = getSingleParameterScore(ingredients, allocations, Type.CALORIES);
            if (calories != 500) {
                continue;
            }
            int sum = calculateSum(ingredients, allocations);
            if (sum > maxSum) {
                maxSum = sum;
            }
        }
        return maxSum;
    }


}
