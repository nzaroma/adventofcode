package ru.roman.year2015.day15;

public enum Type {
    CAPACITY,
    DURABILITY,
    FLAVOR,
    TEXTURE,
    CALORIES
}
