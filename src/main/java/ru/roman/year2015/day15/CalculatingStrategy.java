package ru.roman.year2015.day15;

import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

public interface CalculatingStrategy {
    int apply();

    default int calculateSum(List<Ingredient> ingredients, List<Integer> allocations) {
        int flavor = getSingleParameterScore(ingredients, allocations, Type.FLAVOR);
        int texture = getSingleParameterScore(ingredients, allocations, Type.TEXTURE);
        int capacity = getSingleParameterScore(ingredients, allocations, Type.CAPACITY);
        int durability = getSingleParameterScore(ingredients, allocations, Type.DURABILITY);
        return flavor * texture * capacity * durability;
    }

    default int getSingleParameterScore(List<Ingredient> ingredients, List<Integer> allocations, Type type) {
        return switch (type) {
            case FLAVOR -> calculate(ingredients, Ingredient::flavor, allocations);
            case TEXTURE -> calculate(ingredients, Ingredient::texture, allocations);
            case CAPACITY -> calculate(ingredients, Ingredient::capacity, allocations);
            case DURABILITY -> calculate(ingredients, Ingredient::durability, allocations);
            case CALORIES -> calculate(ingredients, Ingredient::calories, allocations);
        };
    }

    private int calculate(List<Ingredient> ingredients, Function<Ingredient, Integer> function, List<Integer> allocations) {
        int sum = IntStream.range(0, ingredients.size())
                .map(i -> {
                    Ingredient ingredient = ingredients.get(i);
                    Integer count = allocations.get(i);
                    int result = function.apply(ingredient) * count;
                    return result;
                }).sum();
        return Math.max(sum, 0);
    }
}
