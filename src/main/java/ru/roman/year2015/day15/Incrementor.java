package ru.roman.year2015.day15;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;

public class Incrementor implements Iterator<List<Integer>> {

    private int size;
    private int counter;
    private List<Integer> last;
    private int topValue = 100;
    private boolean addMax = false;

    public Incrementor(int size) {
        this.size = size;
        counter = 0;
        last = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            if (i == size - 1) {
                last.add(topValue);
                continue;
            }
            last.add(0);
        }
    }



    @Override
    public boolean hasNext() {
//        System.out.println(counter);
//        return counter <  Math.pow(100, (size - 1));
        return last.get(0) < topValue;
    }

    @Override
    public List<Integer> next() {

        if (counter == 0) {
            counter++;
            return last;
        }
//        last.get(size - 1)

        //find element index to make subtraction
        int subtractionIndex = findSubtractionIndex();
        //find element index to make addition
        int additionIndex = findAdditionIndex(subtractionIndex);
        //prepare new last list
        last = prepareNewList(subtractionIndex, additionIndex);
        counter++;
        return last;
    }

    private int findSubtractionIndex() {
        return IntStream.iterate(size - 1, value -> value != 0, operand -> operand - 1)
                .filter(i -> last.get(i) != 0)
                .findFirst()
                .orElseThrow();
        /**
         * [0, 99, 1]
         * [0, 100, 0]
         * [1, 99, 0]
         * [2, 98, 0]
         */
//        for (int i = size - 1; i >= 0; i--) {
//            Integer lastValueI = last.get(i);
//            if (lastValueI != 0) {
//
//            }
//        }

    }

    private int findAdditionIndex(int subtractIndex) {
        addMax = false;
        if (subtractIndex == size - 1) {
            return subtractIndex - 1;
        }
        int sum = IntStream.rangeClosed(0, size - 2)
                .map(d -> last.get(d))
                .sum();
//        System.out.println(sum);
        if (sum == topValue) {
            addMax = true;
//            return size - 1;

        }
        return subtractIndex - 1;
    }

    private List<Integer> prepareNewList(int subtractionIndex, int additionIndex) {
        List<Integer> newList = new ArrayList<>();
        int sum = 0;
        for (int i = 0; i < size; i++) {
            if (i == additionIndex) {
                if (addMax) {
//                    sum = IntStream.rangeClosed(0, size - 2)
//                            .map(d -> last.get(d))
//                            .sum() - 1 - last.get(0);
//                    last.get(subtractionIndex)
                    sum = topValue - 1 - IntStream.rangeClosed(0, size - 1)
                            .filter(d -> d != subtractionIndex)
                            .map(d -> last.get(d))
                            .sum();
                }
                int newValue = last.get(i) + 1;
                newList.add(newValue);
                continue;

            }
            if (i == subtractionIndex) {
                int newValue;
                if (last.get(i) == sum + 1) {
                    newValue = 0;
                } else {
                    newValue = last.get(i) - 1;
                }
                newList.add(newValue);
                continue;
            }

            newList.add(last.get(i));
        }
        if (sum != 0) {
            newList.set(size - 1, sum);
        }
        return newList;
    }
}
