package ru.roman.year2015.day7;

public class Instruction {
    protected Wire source;
    protected Wire output;

    public Wire getSource() {
        return source;
    }

    public void setSource(Wire source) {
        this.source = source;
    }

    public Wire getOutput() {
        return output;
    }

    public void setOutput(Wire output) {
        this.output = output;
    }

    public Instruction(Wire source, Wire output) {
        this.source = source;
        this.output = output;
    }
}

class DirectInstruction extends Instruction {

    public DirectInstruction(Wire source, Wire output) {
        super(source, output);
    }
}

class NotInstruction extends Instruction {
    private Gate gate = Gate.NOT;

    public NotInstruction(Wire source, Wire output) {
        super(source, output);
    }

    public Gate getGate() {
        return gate;
    }


}

class TwoArgumentInstruction extends Instruction {
    protected Wire secondSource;

    public TwoArgumentInstruction(Wire source, Wire secondSource, Wire output) {
        super(source, output);
        this.secondSource = secondSource;
    }

    public Wire getSecondSource() {
        return secondSource;
    }

    public void setSecondSource(Wire secondSource) {
        this.secondSource = secondSource;
    }
}

class AndInstruction extends TwoArgumentInstruction {
    private Gate gate = Gate.AND;

    public AndInstruction(Wire source, Wire secondSource, Wire output) {
        super(source, secondSource, output);
    }

    public Gate getGate() {
        return gate;
    }
}

class OrInstruction extends TwoArgumentInstruction {
    private Gate gate = Gate.OR;

    public OrInstruction(Wire source, Wire secondSource, Wire output) {
        super(source, secondSource, output);
    }

    public Gate getGate() {
        return gate;
    }
}

class ShiftInstruction extends Instruction {
    protected int shift;

    public int getShift() {
        return shift;
    }

    public void setShift(int shift) {
        this.shift = shift;
    }

    public ShiftInstruction(Wire source, Wire output, int shift) {
        super(source, output);
        this.shift = shift;
    }
}

class RShiftInstruction extends ShiftInstruction {
    private Gate gate = Gate.RSHIFT;

    public RShiftInstruction(Wire source, Wire output, int shift) {
        super(source, output, shift);
    }

    public Gate getGate() {
        return gate;
    }
}

class LShiftInstruction extends ShiftInstruction {
    private Gate gate = Gate.LSHIFT;

    public LShiftInstruction(Wire source, Wire output, int shift) {
        super(source, output, shift);
    }

    public Gate getGate() {
        return gate;
    }
}
