package ru.roman.year2015.day7;

import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class Parser {
    public Instruction parse(String line) {
        Matcher matcher = Pattern.compile("(\\w{1,}) -> (\\w{1,})|(NOT )(.{1,}) -> (\\w{1,})|(.{1,})( AND | RSHIFT | LSHIFT | OR )(.{1,}) -> (\\w{1,})")
                .matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("no matches");
        }
        List<String> arguments = IntStream.range(1, matcher.groupCount() + 1)
                .mapToObj(matcher::group)
                .filter(Objects::nonNull)
                .toList();
        Instruction instruction = createInstruction(arguments);

        return instruction;
    }

    private Instruction createInstruction(List<String> arguments) {
        return switch (arguments.size()) {
            case 2 -> createDirectInstruction(arguments);
            case 3 -> createNotInstruction(arguments);
            case 4 -> create4Instruction(arguments);
            default -> throw new IllegalStateException("Unexpected value: " + arguments.size());
        };
    }

    private Instruction create4Instruction(List<String> matcher) {
        String groupOne = matcher.get(0);
        String groupTwo = matcher.get(1);
        String groupThree = matcher.get(2);
        String groupFour = matcher.get(3);
        Gate gate = determineGate(groupTwo.trim());
        return switch (gate) {
            case RSHIFT -> createRShiftInstruction(groupOne, groupThree, groupFour);
            case LSHIFT -> createLShiftInstruction(groupOne, groupThree, groupFour);
            case OR -> createOrInstruction(groupOne, groupThree, groupFour);
            case AND -> createAndInstruction(groupOne, groupThree, groupFour);
            default -> throw new IllegalStateException("Unexpected value: " + gate);
        };
    }

    private RShiftInstruction createRShiftInstruction(String groupOne, String groupThree, String groupFour) {
        return new RShiftInstruction(createWire(groupOne), createWire(groupFour), Integer.parseInt(groupThree));
    }

    private LShiftInstruction createLShiftInstruction(String groupOne, String groupThree, String groupFour) {
        return new LShiftInstruction(createWire(groupOne), createWire(groupFour), Integer.parseInt(groupThree));
    }

    private OrInstruction createOrInstruction(String groupOne, String groupThree, String groupFour) {
        return new OrInstruction(createWire(groupOne), createWire(groupThree), createWire(groupFour));
    }

    private AndInstruction createAndInstruction(String groupOne, String groupThree, String groupFour) {
        return new AndInstruction(createWire(groupOne), createWire(groupThree), createWire(groupFour));
    }

    private Gate determineGate(String input) {
        return switch (input) {
            case "AND" -> Gate.AND;
            case "RSHIFT" -> Gate.RSHIFT;
            case "LSHIFT" -> Gate.LSHIFT;
            case "OR" -> Gate.OR;
            default -> throw new IllegalStateException("Unexpected value: " + input);
        };
    }

    private Instruction createNotInstruction(List<String> matcher) {
        String groupTwo = matcher.get(1);
        String groupThree = matcher.get(2);
        return new NotInstruction(createWire(groupTwo), createWire(groupThree));
    }

    private Instruction createDirectInstruction(List<String> matcher) {
        String groupOne = matcher.get(0);
        String groupTwo = matcher.get(1);
        return new DirectInstruction(createWire(groupOne), createWire(groupTwo));
    }

    private Wire createWire(String input) {
        if (isNumber(input)) {
            int value = Integer.parseInt(input);
            return new Wire(null, value);
        }
        return new Wire(input, -1);
    }

    private boolean isNumber(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException e) {
            //not a number
            return false;
        }
    }


}
