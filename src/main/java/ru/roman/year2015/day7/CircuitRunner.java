package ru.roman.year2015.day7;

import ru.roman.common.utils.FileReader;

import java.util.List;

public class CircuitRunner {
    public static void main(String[] args) {
        List<String> strings = FileReader.readFile("task7.txt");
        Parser parser = new Parser();
        List<Instruction> instructions = strings.stream()
                .map(parser::parse)
                .toList();
        Calculator calculator = new Calculator();
        int a = calculator.calculateSignal(instructions, "a");
        System.out.println("Result:" + a);
    }
}
