package ru.roman.year2015.day7;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class Calculator {

    private Map<String, Integer> wireNameValues = new HashMap<>();

    public int calculateSignal(List<Instruction> instructions, String wireName) {
        while (true) {
            for (Instruction instruction : instructions) {
                boolean calculate = calculate(instruction);
                if (!calculate) {
                    continue;
                }
                if (instruction.getOutput().name().equalsIgnoreCase(wireName)) {
                    if (instruction.getOutput().value() != -1) {
                        return instruction.getOutput().value();
                    }
                }
            }
        }
    }


    public boolean calculate(Instruction instruction) {
        if (instruction instanceof AndInstruction andInstruction) {
            return calculateAnd(andInstruction);
        }
        if (instruction instanceof OrInstruction orInstruction) {
            return calculateOr(orInstruction);
        }
        if (instruction instanceof LShiftInstruction lshift) {
            return calculateLshift(lshift);
        }
        if (instruction instanceof RShiftInstruction rshift) {
            return calculateRshift(rshift);
        }
        if (instruction instanceof NotInstruction notInstruction) {
            return calculateNot(notInstruction);
        }
        if (instruction instanceof DirectInstruction directInstruction) {
            return calculateDirect(directInstruction);
        }
        return false;
    }

    private boolean calculateDirect(DirectInstruction instruction) {
        int sourceValue = getValue(instruction.getSource());
        if (sourceValue == -1) {
            return false;
        }
        int outputValue = getValue(instruction.getOutput());
        if (outputValue != -1) {
            return true;
        }
        wireNameValues.putIfAbsent(instruction.getOutput().name(), sourceValue);
        instruction.setOutput(new Wire(instruction.getOutput().name(), sourceValue));
        return true;
    }

    private boolean calculateNot(NotInstruction instruction) {
        int sourceValue = getValue(instruction.getSource());
        if (sourceValue == -1) {
            return false;
        }
        int outputValue = getValue(instruction.getOutput());
        if (outputValue != -1) {
            return true;
        }
//        int result = ~instruction.getSource().sourceValue();
        int result = Character.MAX_VALUE - sourceValue;
        wireNameValues.putIfAbsent(instruction.getOutput().name(), result);
        instruction.setOutput(new Wire(instruction.getOutput().name(), result));
        return true;
    }

    private int getValue(Wire wire) {
        if (wire.value() != -1) {
            return wire.value();
        }
        return wireNameValues.getOrDefault(wire.name(), -1);
    }

    private boolean calculateOr(OrInstruction instruction) {
        return calculateTwoArgument(instruction, (integer, integer2) -> integer | integer2);
    }

    private boolean calculateAnd(AndInstruction instruction) {
        return calculateTwoArgument(instruction, (integer, integer2) -> integer & integer2);
    }

    private boolean calculateTwoArgument(TwoArgumentInstruction instruction, BiFunction<Integer, Integer, Integer> function) {
        int sourceValue = getValue(instruction.getSource());
        int secondSourceValue = getValue(instruction.getSecondSource());
        if (sourceValue == -1 || secondSourceValue == -1) {
            return false;
        }
        int outputValue = getValue(instruction.getOutput());
        if (outputValue != -1) {
            return true;
        }
        int result = function.apply(sourceValue, secondSourceValue);
        wireNameValues.putIfAbsent(instruction.getOutput().name(), result);
        Wire newOutput = new Wire(instruction.getOutput().name(), result);
        instruction.setOutput(newOutput);
        return true;
    }

    private boolean calculateRshift(RShiftInstruction rshift) {
        return calculateShift(rshift, (integer, integer2) -> integer >> integer2);
    }

    private boolean calculateLshift(LShiftInstruction lshift) {
        return calculateShift(lshift, (integer, integer2) -> integer << integer2);
    }

    private boolean calculateShift(ShiftInstruction instruction, BiFunction<Integer, Integer, Integer> function) {
        int sourceValue = getValue(instruction.getSource());
        if (sourceValue == -1) {
            return false;
        }
        int outputValue = getValue(instruction.getOutput());
        if (outputValue != -1) {
            return true;
        }
        int shift = instruction.getShift();
        int result = function.apply(sourceValue, shift);
        wireNameValues.putIfAbsent(instruction.getOutput().name(), result);
        Wire newOutput = new Wire(instruction.getOutput().name(), result);
        instruction.setOutput(newOutput);
        return true;
    }


}
