package ru.roman.year2015.day7;

public enum Gate {
    NOT,
    RSHIFT,
    LSHIFT,
    OR,
    AND;
}
