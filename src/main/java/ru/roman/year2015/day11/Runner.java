package ru.roman.year2015.day11;

public class Runner {
    public static void main(String[] args) {
        PasswordGenerator passwordGenerator = new PasswordGenerator(new WordFilter());
        String hxbxwxba = passwordGenerator.generateNextValidPassword("hxbxwxba");
        System.out.println(hxbxwxba);
        String result = passwordGenerator.generateNextValidPassword(hxbxwxba);
        System.out.println(result);
    }
}
