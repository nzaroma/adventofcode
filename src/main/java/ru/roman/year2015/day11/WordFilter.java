package ru.roman.year2015.day11;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordFilter {

    private List<String> threeCombinations;

    public WordFilter() {
        threeCombinations = new ArrayList<>();
        for (int i = 'a'; i < 'z' - 1; i++) {
            threeCombinations.add(String.valueOf((char)i) + String.valueOf((char)(i + 1)) + String.valueOf((char)(i + 2)));
        }
    }

    public boolean isValid(String input) {
        return withoutProhibitedSymbols(input) && withPairs(input) && withThreeIncreasingLetters(input);
    }

    private boolean withThreeIncreasingLetters(String input) {
        return threeCombinations.stream()
                .anyMatch(input::contains);
    }

    private boolean withoutProhibitedSymbols(String input) {
        String pattern = "[ilo]";
        Matcher matcher = Pattern.compile(pattern)
                .matcher(input);
        return !matcher.find();
    }

    private boolean withPairs(String input) {
        String pattern = "((\\w)\\2)";
        List<String> strings = Pattern.compile(pattern)
                .matcher(input)
                .results()
                .map(MatchResult::group)
                .toList();
        return strings.size() >= 2;
    }
}
