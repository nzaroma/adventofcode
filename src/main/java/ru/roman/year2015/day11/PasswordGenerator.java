package ru.roman.year2015.day11;

import java.util.stream.IntStream;

public class PasswordGenerator {

    private WordFilter filter;

    public PasswordGenerator(WordFilter filter) {
        this.filter = filter;
    }

    public String generateNextPassword(String initial) {
        char[] chars = initial.toCharArray();
        boolean add = false;
        for (int i = chars.length - 1; i >= 0; i--) {
            add = false;
            char c = chars[i];
            if (c == 'z') {
                chars[i] = 'a';
                add = true;
            } else {
                chars[i] = (char) (c + 1);
            }
            if (!add) {
                break;
            }
        }
        return String.valueOf(chars);
    }

    public String generateNextValidPassword(String initial) {
        String result = initial;
        do {
            result = generateNextPassword(result);
        } while (!filter.isValid(result));
        return result;
    }


}
