package ru.roman.year2015.day10;

public class LookAndSay {
    public static void main(String[] args) {
        String input = "3113322113";
        LookAndSay lookAndSay = new LookAndSay();
        for (int i = 0; i < 50; i++) {
            input = lookAndSay.transform(input);
//            System.out.println(input);
        }
        System.out.println(input.length());
    }

    public int transformNTimes(String input, int times) {
        String result = input;
        for (int i = 0; i < times; i++) {
            result = transform(result);
        }
        return result.length();
    }

    public String transform(String input) {
        char previousChar = Character.MAX_VALUE;
        int sum = 0;
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (isLast(input, i)) {
                if (previousChar == Character.MAX_VALUE) {
                    sum = 1;
                    result.append(sum).append(c);
                }
                else if (c != previousChar) {
                    result.append(sum).append(previousChar);
                    sum = 1;
                    result.append(sum).append(c);
                } else {
                    sum++;
                    result.append(sum).append(c);
                }
                break;
            }
            if (isFirst(i)) {
                sum++;
                previousChar = c;
                continue;
            }
            if (c == previousChar) {
                sum++;
                continue;
            } else {
                result.append(sum).append(previousChar);
                sum = 1;
                previousChar = c;
            }
        }
        return result.toString();
    }

    private boolean isFirst(int i) {
        return i == 0;
    }

    private boolean isLast(String input, int i) {
        return i == input.length() - 1;
    }
}
