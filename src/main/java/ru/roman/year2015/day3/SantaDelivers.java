package ru.roman.year2015.day3;

import ru.roman.common.utils.FileReader;

import java.util.*;
import java.util.function.Function;

public class SantaDelivers {
    class SantaCell {
        private int x;
        private int y;


        public SantaCell(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SantaCell santaCell = (SantaCell) o;
            return x == santaCell.x && y == santaCell.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }

    private Function<String, Integer> function = this::calculateReduce;

    public Function<String, Integer> getFunction() {
        return function;
    }

    public static void main(String[] args) {
        int i = new SantaDelivers().calculateAtLeastOnePresentFromFile("task3.txt");
        System.out.println(i);
        int i2 = new SantaDelivers().calculateRoboFromFile("task3.txt");
        System.out.println(i2);
        int i3 = new SantaDelivers().calculate("task3.txt");
        System.out.println(i3);
    }

    public int calculateAtLeastOnePresentFromFile(String fileName) {
        List<String> strings = FileReader.readFile(fileName);
        String directions = Objects.requireNonNull(strings).stream().reduce((finalString, line) -> finalString + line).orElse("");
        return calculateAtLeastOnePresent(directions);
    }

    public int calculateRoboFromFile(String fileName) {
        List<String> strings = FileReader.readFile(fileName);
        String directions = Objects.requireNonNull(strings).stream().reduce((finalString, line) -> finalString + line).orElse("");
        return calculateWithRoboSanta(directions);
    }

    public int calculate(String fileName) {
        List<String> strings = FileReader.readFile(fileName);
        String directions = Objects.requireNonNull(strings).stream().reduce((finalString, line) -> finalString + line).orElse("");
        return function.apply(directions);
    }

    public int calculateAtLeastOnePresent(String line) {
        SantaCell initialCell = new SantaCell(0, 0);
        LinkedList<SantaCell> cells = new LinkedList<>();
        cells.add(initialCell);
        cells.add(initialCell);
        for (char c : line.toCharArray()) {
            SantaCell lastCell = cells.getLast();
            SantaCell currentCell = move(c, lastCell);
            cells.add(currentCell);
        }
        HashSet<SantaCell> santaCells = new HashSet<>(cells);
        return santaCells.size();
    }

    public int calculateWithRoboSanta(String line) {
        SantaCell initialCell = new SantaCell(0, 0);
        LinkedList<SantaCell> santaCells = new LinkedList<>();
        LinkedList<SantaCell> roboCells = new LinkedList<>();
        santaCells.add(initialCell);
        roboCells.add(initialCell);
        for (int i = 0; i < line.length(); i++) {
            if (i % 2 == 0) {
                SantaCell lastCell = santaCells.getLast();
                SantaCell currentCell = move(line.charAt(i), lastCell);
                santaCells.add(currentCell);
            } else {
                SantaCell lastCell = roboCells.getLast();
                SantaCell currentCell = move(line.charAt(i), lastCell);
                roboCells.add(currentCell);
            }
        }

        HashSet<SantaCell> cells = new HashSet<>(santaCells);
        cells.addAll(roboCells);
        return cells.size();
    }

    public SantaCell move(int direction, SantaCell lastCell) {
        return switch (direction) {
            case '>' -> new SantaCell(lastCell.getX() + 1, lastCell.getY());
            case '<' -> new SantaCell(lastCell.getX() - 1, lastCell.getY());
            case '^' -> new SantaCell(lastCell.getX(), lastCell.getY() + 1);
            case 'v' -> new SantaCell(lastCell.getX(), lastCell.getY() - 1);
            default -> throw new IllegalStateException("Unexpected value: " + direction);
        };
    }

    public int calculateReduce(String line) {
        SantaCell initialCell = new SantaCell(0, 0);
        List<SantaCell> cells = new ArrayList<>();
        cells.add(initialCell);
        List<SantaCell> allCells = line.chars().boxed()
                .reduce(cells,
                        (santaCells, direction) -> {
                            SantaCell previousCell = santaCells.get(santaCells.size() - 1);
                            santaCells.add(move(direction, previousCell));
                            return santaCells;
                        }, (santaCells, santaCells2) -> {
                            santaCells.addAll(santaCells2);
                            return santaCells;
                        });
        Set<SantaCell> uniqueCells = new HashSet<>(allCells);
        return uniqueCells.size();
    }
}
