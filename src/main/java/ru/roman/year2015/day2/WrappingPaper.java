package ru.roman.year2015.day2;

import ru.roman.common.utils.FileReader;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * --- Day 2: I Was Told There Would Be No Math ---
 * The elves are running low on wrapping paper, and so they need to submit an order for more. They have a list of the dimensions (length l, width w, and height h) of each present, and only want to order exactly as much as they need.
 *
 * Fortunately, every present is a box (a perfect right rectangular prism), which makes calculating the required wrapping paper for each gift a little easier: find the surface area of the box, which is 2*l*w + 2*w*h + 2*h*l. The elves also need a little extra paper for each present: the area of the smallest side.
 *
 * For example:
 *
 * A present with dimensions 2x3x4 requires 2*6 + 2*12 + 2*8 = 52 square feet of wrapping paper plus 6 square feet of slack, for a total of 58 square feet.
 * A present with dimensions 1x1x10 requires 2*1 + 2*10 + 2*10 = 42 square feet of wrapping paper plus 1 square foot of slack, for a total of 43 square feet.
 * All numbers in the elves' list are in feet. How many total square feet of wrapping paper should they order?
 */
public class WrappingPaper {
    public static void main(String[] args) {
        WrappingPaper w = new WrappingPaper();
        int i = w.calculateFromFile("task2.txt");
        System.out.println(i);
        int i2 = w.calculateRibbonFromFile("task2.txt");
        System.out.println(i2);
    }

    public int calculateOnePresent(int[] dimensions) {
        if (dimensions.length != 3) throw new IllegalArgumentException("Wrong dimensions");
        Arrays.sort(dimensions);
        return 2 * dimensions[0] * dimensions[1] + 2 * dimensions[1] * dimensions[2] + 2 * dimensions[2] * dimensions[0] + dimensions[0]*dimensions[1];
    }

    public int calculateTotal(List<String> list) {
        return getStream(list)
                .mapToInt(this::calculateOnePresent)
                .sum();
    }

    public int calculateFromFile(String fileName) {
        List<String> strings = FileReader.readFile(fileName);
        return calculateTotal(strings);
    }

    public int calculateOneRibbon(int[] dimensions) {
        Arrays.sort(dimensions);
        return 2 * dimensions[0] + 2 * dimensions[1] + dimensions[0] * dimensions[1] * dimensions[2];
    }

    public int calculateRibbonFromFile(String fileName) {
        List<String> strings = FileReader.readFile(fileName);
        return calculateRibbonTotal(strings);
    }

    public int calculateRibbonTotal(List<String> list) {
        return getStream(list)
                .mapToInt(this::calculateOneRibbon)
                .sum();
    }

    public Stream<int[]> getStream(List<String> list) {
        return list.stream()
                .map(line -> line.split("x"))
                .map(lineArray -> new int[]{Integer.parseInt(lineArray[0]), Integer.parseInt(lineArray[1]), Integer.parseInt(lineArray[2])});
    }
}
