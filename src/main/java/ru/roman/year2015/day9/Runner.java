package ru.roman.year2015.day9;

import ru.roman.common.utils.FileReader;

import java.util.List;

public class Runner {
    public static void main(String[] args) {
        List<String> strings = FileReader.readFile("task9.txt");
        Parser parser = new Parser();
        List<Path> paths = strings.stream().map(parser::parse).toList();
        Calculator calculator = new Calculator();
        calculator.calculateShortestRoute(paths);
    }
}
