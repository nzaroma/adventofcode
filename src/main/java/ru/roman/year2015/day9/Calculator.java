package ru.roman.year2015.day9;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class Calculator {
    public int calculateShortestRoute(List<Path> paths) {
        List<Path> extractedPaths = paths.stream()
                .flatMap(d -> extractPaths(d).stream())
                .toList();
        List<String> cities = paths.stream()
                .flatMap(d -> Stream.of(d.from(), d.to()))
                .distinct()
                .toList();

        List<List<String>> result = new ArrayList<>();
        divide(cities, new ArrayList<>(), result);
        List<PathDistance> pathDistances = result.stream()
                .map(list -> new PathDistance(list, calculateRoute(list, extractedPaths)))
                .sorted(Comparator.comparingInt(PathDistance::distance))
                .toList();
        System.out.println(pathDistances.get(0));
        System.out.println(pathDistances.get(pathDistances.size() - 1));
        return 0;
    }

    private int calculateRoute(List<String> cities, List<Path> paths) {
        int distance = 0;
        for (int i = 0; i < cities.size() - 1; i++) {
            String from = cities.get(i);
            String to = cities.get(i + 1);
            Path foundPath = paths.stream().filter(path -> path.from().equalsIgnoreCase(from) && path.to().equalsIgnoreCase(to)).findAny().orElseThrow(IllegalArgumentException::new);
            distance += foundPath.distance();
        }
        return distance;
    }




    private List<Path> extractPaths(Path path) {
        return List.of(path, new Path(path.to(), path.from(), path.distance()));
    }

    private void divide(List<String> cities, List<String> intermediateCities, List<List<String>> resultList) {
        if (cities.size() == 1) {
            intermediateCities.add(cities.get(0));
            resultList.add(intermediateCities);
            return;
        }
        for (int i = 0; i < cities.size(); i++) {
            ArrayList<String> newIntermediate = new ArrayList<>(intermediateCities);
            ArrayList<String> localCities = new ArrayList<>(cities);
            String removed = localCities.remove(i);
            newIntermediate.add(removed);
            divide(localCities, newIntermediate, resultList);
        }
    }

}
/*
1234
1243
1324
1342
1423
1432
*/