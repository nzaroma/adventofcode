package ru.roman.year2015.day9;

public record Path(String from, String to, int distance) {
}
