package ru.roman.year2015.day9;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
//    Tristram to Arbre = 14
    public Path parse(String line) {
        Matcher matcher = Pattern.compile("(\\w*) to (\\w*) = (\\d*)").matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("no matches");
        }
        String from = matcher.group(1);
        String to = matcher.group(2);
        int distance = Integer.parseInt(matcher.group(3));
        return new Path(from, to, distance);
    }
}
