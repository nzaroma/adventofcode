package ru.roman.year2015.day9;

import java.util.List;

public record PathDistance(List<String> cities, int distance) {
}
