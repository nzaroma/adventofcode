package ru.roman.year2015.day6;

import ru.roman.common.utils.FileReader;

import java.util.Arrays;
import java.util.List;

public class FireHazard {
    public static void main(String[] args) {
        List<String> strings = FileReader.readFile("task6.txt");
        FireHazard fireHazard = new FireHazard();
        Cell[][] cells = fireHazard.init();
        fireHazard.applyCommands(strings, cells, new BasicLitStrategy());
        System.out.println(fireHazard.calculateLitCells(cells));

        cells = fireHazard.init();
        fireHazard.applyCommands(strings, cells, new BrightenStrategy());
        System.out.println(fireHazard.calculateLitCells(cells));
    }

    public Cell[][] init() {
        Cell[][] cells = new Cell[1000][1000];
        for (int i = 0; i < cells[0].length; i++) {
            for (int j = 0; j < cells.length; j++) {
                cells[i][j] = new Cell(i, j, 0);
            }
        }
        return cells;
    }

    public void applyCommands(List<String> lines, Cell[][] cells, LitStrategy strategy) {
        Parser parser = new Parser();
        lines.stream()
                .map(parser::parse)
                .forEach(command -> applyCommand(command, cells, strategy));
    }

    public void applyCommand(Command command, Cell[][] cells, LitStrategy strategy) {
        for (int i = command.startX(); i <= command.finishX(); i++) {
            for (int j = command.startY(); j <= command.finishY(); j++) {
                Cell cell = cells[i][j];
                strategy.apply(command.action(), cell);
            }
        }
    }

    public long calculateLitCells(Cell[][] cells) {
        return Arrays.stream(cells)
                .flatMap(Arrays::stream)
                .mapToInt(Cell::getState)
                .sum();
    }
}
