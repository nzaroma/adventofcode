package ru.roman.year2015.day6;

public record Command(Action action, int startX, int startY, int finishX, int finishY) {
}
