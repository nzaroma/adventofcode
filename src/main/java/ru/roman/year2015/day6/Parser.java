package ru.roman.year2015.day6;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

    public Command parse(String line) {
        /**
         * () - group
         *  | - or
         *  \s - any whitespace character
         *  [0-9] - one of number
         *  {1,3} - 1..3 occasions
         *  ?: - java specific? ignore group, so in results we dont need to sort out indexes
         */
        Matcher matcher = Pattern.compile("(turn on|turn off|toggle)\\s([0-9]{1,3})(?:,)([0-9]{1,3})\\s(?:through )([0-9]{1,3})(?:,)([0-9]{1,3})")
                .matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("no matches");
        }
        Action action = getAction(matcher.group(1));
        int startX = Integer.parseInt(matcher.group(2));
        int startY = Integer.parseInt(matcher.group(3));
        int finishX = Integer.parseInt(matcher.group(4));
        int finishY = Integer.parseInt(matcher.group(5));
        return new Command(action, startX, startY, finishX, finishY);

    }

    private Action getAction(String line) {
        return switch (line) {
            case "turn on" -> Action.TURN_ON;
            case "turn off" -> Action.TURN_OFF;
            case "toggle" -> Action.TOGGLE;
            default -> throw new IllegalArgumentException();
        };
    }
}
