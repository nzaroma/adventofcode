package ru.roman.year2015.day6;

public enum Action {

    TURN_ON,
    TURN_OFF,
    TOGGLE;

}
