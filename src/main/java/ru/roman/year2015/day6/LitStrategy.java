package ru.roman.year2015.day6;

public interface LitStrategy {
    void apply(Action action, Cell cell);
}

class BasicLitStrategy implements LitStrategy {

    @Override
    public void apply(Action action, Cell cell) {
        switch (action) {
            case TURN_ON -> cell.setState(1);
            case TURN_OFF -> cell.setState(0);
            case TOGGLE -> {
                int state = cell.getState() == 1 ? 0 : 1;
                cell.setState(state);
            }
        }
    }
}

class BrightenStrategy implements LitStrategy {

    @Override
    public void apply(Action action, Cell cell) {
        final int state = cell.getState();
        switch (action) {
            case TURN_ON -> cell.setState(state + 1);
            case TURN_OFF -> {
                if (state > 0) cell.setState(state - 1);
            }
            case TOGGLE -> cell.setState(state + 2);

        }
    }
}
