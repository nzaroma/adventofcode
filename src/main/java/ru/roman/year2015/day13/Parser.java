package ru.roman.year2015.day13;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
    public Connection parseLine(String line) {
        Matcher matcher = Pattern.compile("(\\w*) would (gain|lose) (\\d*) happiness units by sitting next to (\\w*).").matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("no matches");
        }
        String name = matcher.group(1);
        String nameFrom = matcher.group(4);
        int value = Integer.parseInt(matcher.group(3));
        Action action = "gain".equalsIgnoreCase(matcher.group(2)) ? Action.GAIN : Action.LOSE;
        return new Connection(name, action, value, nameFrom);
    }
}
