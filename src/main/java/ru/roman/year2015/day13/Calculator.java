package ru.roman.year2015.day13;

import ru.roman.common.utils.ListUtils;

import java.util.ArrayList;
import java.util.List;

public class Calculator {
    public int calculateOptimal(List<Connection> connections) {
        List<String> names = connections.stream().map(Connection::name).distinct().toList();
        List<List<String>> connectionCombinations = ListUtils.getCombinations(names);
        List<Integer> integers = connectionCombinations.stream()
                .map(c -> calculateRelations(c, connections))
                .sorted()
                .toList();
        return integers.get(integers.size() - 1);
    }

    public int calculateOptimalWithMe(List<Connection> connections) {
        List<String> names = connections.stream().map(Connection::name).distinct().toList();
        ArrayList<String> newNames = new ArrayList<>(names);
        newNames.add("Romka");
        List<List<String>> connectionCombinations = ListUtils.getCombinations(newNames);
        List<Integer> integers = connectionCombinations.stream()
                .map(c -> calculateRelations(c, connections))
                .sorted()
                .toList();
        return integers.get(integers.size() - 1);
    }

    private int calculateRelations(List<String> combination, List<Connection> connections) {
        int result = 0;
        for (int i = 0; i < combination.size(); i++) {
            String name;
            String nameTo;
            if (i == combination.size() - 1) {
                name = combination.get(i);
                nameTo = combination.get(0);
            } else {
                name = combination.get(i);
                nameTo = combination.get(i + 1);
            }
            Connection connection = getConnection(name, nameTo, connections);
            if (Action.GAIN == connection.action()) {
                result += connection.value();
            } else {
                result -= connection.value();
            }
            Connection connectionInverted = getConnection(nameTo, name, connections);
            if (Action.GAIN == connectionInverted.action()) {
                result += connectionInverted.value();
            } else {
                result -= connectionInverted.value();
            }

        }
        return result;
    }

    private Connection getConnection(String name, String nameTo, List<Connection> connections) {
        return connections.stream()
                .filter(c -> c.name().equalsIgnoreCase(name))
                .filter(c -> c.nameTo().equalsIgnoreCase(nameTo))
                .findAny()
                .orElseGet(() -> {
                    if ("Romka".equalsIgnoreCase(name)) {
                        return new Connection("Romka", Action.GAIN, 0, nameTo);
                    } else {
                        return new Connection(name, Action.GAIN, 0, "Romka");
                    }
                });
    }
}
