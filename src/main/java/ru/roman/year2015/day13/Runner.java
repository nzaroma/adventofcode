package ru.roman.year2015.day13;

import ru.roman.common.utils.FileReader;

import java.util.List;

public class Runner {
    public static void main(String[] args) {
        List<String> strings = FileReader.readFile("task13.txt");
        Parser parser = new Parser();
        List<Connection> connections = strings.stream()
                .map(d -> parser.parseLine(d))
                .toList();
        Calculator calculator = new Calculator();
        int i = calculator.calculateOptimal(connections);
        System.out.println(i);
        int i1 = calculator.calculateOptimalWithMe(connections);
        System.out.println(i1);

    }
}
