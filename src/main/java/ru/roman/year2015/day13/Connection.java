package ru.roman.year2015.day13;

public record Connection(String name, Action action, int value, String nameTo) {
}
