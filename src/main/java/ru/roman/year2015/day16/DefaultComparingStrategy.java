package ru.roman.year2015.day16;

import java.util.Map;

public class DefaultComparingStrategy implements ComparingStrategy {

    @Override
    public boolean apply(Map.Entry<String, Integer> entry, int value) {
        return value == entry.getValue();
    }
}
