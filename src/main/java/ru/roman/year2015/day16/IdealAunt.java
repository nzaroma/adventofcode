package ru.roman.year2015.day16;

import java.util.Map;

public record IdealAunt(Map<String, Integer> stats) {
}
