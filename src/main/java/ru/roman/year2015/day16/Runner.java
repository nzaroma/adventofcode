package ru.roman.year2015.day16;

import ru.roman.common.utils.CommonParser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Runner {
    public static void main(String[] args) {
        CommonParser<Characteristic> parser = new Parser();
        List<Characteristic> characteristics = parser.parseFile("task16.txt");
//        System.out.println(characteristics);
        /**
         * children: 3
         * cats: 7
         * samoyeds: 2
         * pomeranians: 3
         * akitas: 0
         * vizslas: 0
         * goldfish: 5
         * trees: 3
         * cars: 2
         * perfumes: 1
         */
        Map<String, Integer> map = new HashMap<>();
        map.put("children", 3);
        map.put("cats", 7);
        map.put("samoyeds", 2);
        map.put("pomeranians", 3);
        map.put("akitas", 0);
        map.put("vizslas", 0);
        map.put("goldfish", 5);
        map.put("trees", 3);
        map.put("cars", 2);
        map.put("perfumes", 1);

        IdealAunt idealAunt = new IdealAunt(map);
        AuntStrategyImpl auntStrategy = new AuntStrategyImpl(idealAunt, new DefaultComparingStrategy());
        int apply = auntStrategy.apply(characteristics);
        System.out.println(apply);

        AuntStrategyImpl auntStrategy2 = new AuntStrategyImpl(idealAunt, new AdvancedComparingStrategy());
        int apply2 = auntStrategy2.apply(characteristics);
        System.out.println(apply2);
    }
}
