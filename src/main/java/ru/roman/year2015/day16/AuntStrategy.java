package ru.roman.year2015.day16;

import java.util.List;

public interface AuntStrategy {
    int apply(List<Characteristic> characteristics);
}
