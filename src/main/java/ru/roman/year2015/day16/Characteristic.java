package ru.roman.year2015.day16;

public record Characteristic(int id, String arg1, int value1, String arg2, int value2, String arg3, int value3) {
}
