package ru.roman.year2015.day16;

import ru.roman.common.utils.CommonParser;

public class Parser implements CommonParser<Characteristic> {

    @Override
    public String getPattern() {
        return "Sue (?<id>\\d*): (?<arg1>\\w*): (?<value1>\\d*), (?<arg2>\\w*): (?<value2>\\d*), (?<arg3>\\w*): (?<value3>\\d*)";
    }

    @Override
    public Class<Characteristic> getClazz() {
        return Characteristic.class;
    }

}
