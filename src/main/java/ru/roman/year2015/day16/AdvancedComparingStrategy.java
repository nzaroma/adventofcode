package ru.roman.year2015.day16;

import java.util.Map;

public class AdvancedComparingStrategy implements ComparingStrategy{
    @Override
    public boolean apply(Map.Entry<String, Integer> entry, int value) {
        String key = entry.getKey();
        if (key.equalsIgnoreCase("cats") || key.equalsIgnoreCase("trees")) {
            return value > entry.getValue();
        }
        if (key.equalsIgnoreCase("pomeranians") || key.equalsIgnoreCase("goldfish")) {
            return value < entry.getValue();
        }
        return value == entry.getValue();
    }
}
