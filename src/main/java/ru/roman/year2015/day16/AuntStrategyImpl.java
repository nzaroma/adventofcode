package ru.roman.year2015.day16;

import java.util.List;
import java.util.Map;

public class AuntStrategyImpl implements AuntStrategy {

    private IdealAunt idealAunt;
    private ComparingStrategy strategy;

    public AuntStrategyImpl(IdealAunt idealAunt, ComparingStrategy strategy) {
        this.idealAunt = idealAunt;
        this.strategy = strategy;
    }

    @Override
    public int apply(List<Characteristic> characteristics) {
        Map<String, Integer> stats = idealAunt.stats();
        return characteristics.stream()
                .filter(this::isAuntOk)
                .map(Characteristic::id)
                .findAny()
                .orElseThrow();
    }

    private boolean isAuntOk(Characteristic characteristic) {
        Map<String, Integer> stats = idealAunt.stats();
        return stats.entrySet().stream()
                .allMatch(entry -> doesAuntHaveIt(characteristic, entry));
    }

    private boolean doesAuntHaveIt(Characteristic characteristic, Map.Entry<String, Integer> entry) {
        String key = entry.getKey();
//        if (characteristic.arg1().equalsIgnoreCase(key)) {
//            return characteristic.value1() == entry.getValue();
//        } else if (characteristic.arg2().equalsIgnoreCase(key)) {
//            return characteristic.value2() == entry.getValue();
//        } else if (characteristic.arg3().equalsIgnoreCase(key)) {
//            return characteristic.value3() == entry.getValue();
//        }
        if (characteristic.arg1().equalsIgnoreCase(key)) {
            return strategy.apply(entry, characteristic.value1());
        } else if (characteristic.arg2().equalsIgnoreCase(key)) {
            return strategy.apply(entry, characteristic.value2());
        } else if (characteristic.arg3().equalsIgnoreCase(key)) {
            return strategy.apply(entry, characteristic.value3());
        }
        return true;
    }
}
