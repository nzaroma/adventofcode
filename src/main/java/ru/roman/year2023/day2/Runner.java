package ru.roman.year2023.day2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import ru.roman.common.utils.CommonParser;

public class Runner {

    public static void main(String[] args) {

        int redLimit = 12;
        int greenLimit = 13;
        int blueLimit = 14;
        CommonParser<Game> parser = new GameParser();
        List<Game> gameData = parser.parseFile("2023/task2.txt");
        List<GameData> allData = gameData.stream()
                .map(game -> {
                    List<List<CubeData>> cubeData = new ArrayList<>();
                    String[] split = game.data().split(";");
                    for (String cubeList : split) {
                        List<CubeData> singleCubeData = new ArrayList<>();
                        cubeData.add(singleCubeData);
                        String[] cubes = cubeList.trim().split(",");
                        for (String cube : cubes) {
                            String[] cubeDefinition = cube.trim().split(" ");
                            CubeData innerCubeData = new CubeData(Integer.parseInt(cubeDefinition[0]),
                                    CubeType.findTypeForColor(cubeDefinition[1]));
                            singleCubeData.add(innerCubeData);
                        }
                    }
                    return new GameData(game.id(), cubeData);
                }).collect(Collectors.toList());
        int sum = allData.stream()
                .filter(game -> {
                    List<List<CubeData>> games = game.games();
                    return games.stream()
                            .allMatch(cubeData -> cubeData.stream()
                                    .allMatch(data -> {
                                        if (data.cubeType() == CubeType.BLUE) {
                                            return data.number() <= blueLimit;
                                        } else if (data.cubeType() == CubeType.GREEN) {
                                            return data.number() <= greenLimit;
                                        } else {
                                            return data.number() <= redLimit;
                                        }
                                    }));

                })
                .mapToInt(game -> game.id())
                .sum();
        System.out.println(sum);

        int sum1 = allData.stream()
                .mapToInt(game -> {
                    List<CubeData> cubeData = game.games().stream()
                            .flatMap(Collection::stream)
                            .toList();
                    int blue = getCubeNumber(cubeData, CubeType.BLUE);
                    int red = getCubeNumber(cubeData, CubeType.RED);
                    int green = getCubeNumber(cubeData, CubeType.GREEN);

                    return blue * red * green;
                }).sum();
        System.out.println(sum1);
    }

    private static int getCubeNumber(List<CubeData> cubeData, CubeType color) {

        return cubeData.stream()
                .filter(data -> data.cubeType() == color)
                .sorted((o1, o2) -> Integer.compare(o2.number(), o1.number()))
                .map(CubeData::number)
                .findFirst()
                .orElse(1);

    }

}
