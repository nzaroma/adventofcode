package ru.roman.year2023.day2;

import java.util.Arrays;

public record CubeData(int number, CubeType cubeType) {

}

enum CubeType {
    BLUE("blue"), GREEN("green"), RED("red");
    private final String color;

    CubeType(String color) {

        this.color = color;
    }

    public String getColor() {

        return color;
    }

    public static CubeType findTypeForColor(String inputColor) {

        return Arrays.stream(CubeType.values()).filter(cubeType -> cubeType.getColor().equals(inputColor)).findAny()
                .orElse(null);
    }
}
