package ru.roman.year2023.day2;

import ru.roman.common.utils.CommonParser;

public class GameParser implements CommonParser<Game> {

    @Override
    public String getPattern() {

        return "Game\\s(?<id>\\d+):(?<data>.*)";
    }

    @Override
    public Class<Game> getClazz() {

        return Game.class;
    }
}
