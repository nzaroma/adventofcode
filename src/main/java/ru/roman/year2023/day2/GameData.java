package ru.roman.year2023.day2;

import java.util.List;

public record GameData(int id, List<List<CubeData>> games) {
}


