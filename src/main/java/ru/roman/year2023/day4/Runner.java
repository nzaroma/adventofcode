package ru.roman.year2023.day4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import ru.roman.common.utils.FileReader;

public class Runner {

    public static void main(String[] args) {
        List<String> lines = FileReader.readFile("2023/task4.txt");

        int result = 0;
        for (String line : lines) {
            String[] split = line.split("\\|");
            System.out.println(split[0] + " " + split[1]);
            List<String> winNumbers = new ArrayList<>(Arrays.stream(split[0].split(":")[1].trim().split(" ")).toList());
            List<String> myNumbers = Arrays.stream(split[1].trim().split(" ")).toList().stream().filter(s -> !s.isEmpty()).toList();
            winNumbers.retainAll(myNumbers);
            int pow = (int) Math.pow(2, winNumbers.size() - 1);
            result += pow;
        }
        System.out.println(result);

        Map<Integer, Integer> cards = new HashMap<>();
        int currentLine = 0;
        for (String line : lines) {
            currentLine++;
            cards.compute(currentLine, (key, value) -> value == null ? 1 : value + 1);
            String[] split = line.split("\\|");
            System.out.println(split[0] + " " + split[1]);
            List<String> winNumbers = new ArrayList<>(Arrays.stream(split[0].split(":")[1].trim().split(" ")).toList());
            List<String> myNumbers = Arrays.stream(split[1].trim().split(" ")).toList().stream().filter(s -> !s.isEmpty()).toList();
            winNumbers.retainAll(myNumbers);
            System.out.println("Line " + currentLine + " win numbers: " + winNumbers.size());
            Integer times = cards.get(currentLine);
            IntStream.rangeClosed(currentLine + 1, currentLine + winNumbers.size())
                    .forEach(i -> cards.compute(i, (key, value) -> value == null ? 1 : value + times));
            System.out.println(cards);
        }

        System.out.println(cards);
        int sum = cards.values().stream().mapToInt(value -> value).sum();
        System.out.println(sum);
    }

}
