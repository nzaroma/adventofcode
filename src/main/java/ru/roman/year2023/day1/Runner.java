package ru.roman.year2023.day1;

import ru.roman.common.utils.FileReader;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import static java.lang.Math.min;

public class Runner {
    public static void main(String[] args) {
        List<String> lines = FileReader.readFile("2023/task1.txt");
        List<String> lines2 = Arrays.stream("""
                two1nine
                eightwothree
                abcone2threexyz
                xtwone3four
                4nineeightseven2
                zoneight234
                7pqrstsixteen""".split("\n")).toList();
        int sum = lines.stream()
                .map(line -> {
                    String result = parseLine(line);
                    if (result.length() == 1) {
                        return result + result;
                    }
                    String mine =  "" + result.charAt(0) + result.charAt(result.length() - 1);
                    String theirs = getCalibNums(line);
                    if (!mine.equals(theirs)) {
                        System.out.println("Line: " + line + ", mine: " + mine + " , theirs: " + theirs);
                    }
                    return theirs;
                })
                .map(Runner::getCalibNums)
                .mapToInt(Integer::parseInt)
//                .peek(System.out::println)
                .sum();

        System.out.println(sum
        );
    }

    private static String parseLine(String input) {

        String result = "";
        for (int i = 0; i < input.length(); i++) {
            result += input.charAt(i);
            result = result.replaceAll("one", "1");
            result = result.replaceAll("two", "2");
            result = result.replaceAll("three", "3");
            result = result.replaceAll("four", "4");
            result = result.replaceAll("five", "5");
            result = result.replaceAll("six", "6");
            result = result.replaceAll("seven", "7");
            result = result.replaceAll("eight", "8");
            result = result.replaceAll("nine", "9");
//            result = result.replaceAll("zero", "0");
        }
        result = result.replaceAll("[a-zA-Z]", "");
        if (result.isEmpty()) {
            System.err.println(result);
        }
//        System.out.println(result);
        return result;
    }

    private static String getCalibNums(String line){
        String[] digits = {"-", "one", "two", "three", "four", "five", "six",
                "seven", "eight", "nine"};
        var nums = "";
        var chars = line.toCharArray();
        for(int i = 0; i < chars.length; i++){
            if(Character.isDigit(chars[i])){
                nums += chars[i];
                continue;
            }
            var substr = line.substring(i, min(5 + i, line.length()));
            for(int j = 0; j < digits.length; j++){
                if(substr.startsWith(digits[j])){
                    i += digits[j].length() - 2;
                    nums += j;
                    break;
                }
            }
        }
        return "" + nums.charAt(0) + nums.charAt(nums.length() - 1);
    }
}
