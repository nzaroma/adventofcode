package ru.roman.year2023.day3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.roman.common.utils.FileReader;

public class Runner {

    public static void main(String[] args) {

        List<String> test = new ArrayList<>();
        String testInput = """
                467..114..
                ...*......
                ..35..633.
                ......#...
                617*......
                .....+.58.
                ..592.....
                ......755.
                ...$.*....
                .664.598..
                 """;
        for (String s : testInput.split("\n")) {
            test.add(s);
        }

//        List<String> lines = test;
        List<String> lines = FileReader.readFile("2023/task3.txt");
        int linesCount = lines.size();
        int lineLength = lines.get(0).length();

//        task1(linesCount, lineLength, lines);
        task2(linesCount, lineLength, lines);

//        ***************

    }

    private static void task1(int linesCount, int lineLength, List<String> lines) {

        boolean[][] includeMatrix = new boolean[linesCount][lineLength];

        for (int i = 0; i < linesCount; i++) {
            String line = lines.get(i);
            for (int j = 0; j < lineLength; j++) {
                char currentChar = line.charAt(j);
                boolean digit = Character.isDigit(currentChar);
                boolean dot = currentChar == '.';
                if (!digit && !dot) {
                    populateMatrix(includeMatrix, i, j);
                }
            }
        }
//        for (boolean[] matrix : includeMatrix) {
//            for (boolean b : matrix) {
//                System.out.print(b ? "+" : " ");
//            }
//            System.out.println();
//        }

        List<Integer> numbers = new ArrayList<>();
        boolean prevNumber = false;
        boolean include = false;
        String number = "";
        for (int i = 0; i < linesCount; i++) {
            String line = lines.get(i);
            for (int j = 0; j < lineLength; j++) {
                char currentChar = line.charAt(j);
//                System.out.print(currentChar);
                boolean digit = Character.isDigit(currentChar);
                if (digit) {
                    number = number + currentChar;
                    include = include || includeMatrix[i][j];
                } else {
                    if (include) {
                        numbers.add(Integer.parseInt(number));
                    }
                    include = false;
                    number = "";
                }
            }
            if (include) {
                numbers.add(Integer.parseInt(number));
            }
//            System.out.println();
            include = false;
            number = "";
        }
        System.out.println(numbers);
        int sum = numbers.stream().mapToInt(Integer::intValue).sum();
        System.out.println(sum);
    }

    private static void task2(int linesCount, int lineLength, List<String> lines) {

//        boolean[][] starMatrix = new boolean[linesCount][lineLength];
        int[][] starMatrixInt = new int[linesCount][lineLength];
        int index = 0;

        for (int i = 0; i < linesCount; i++) {
            String line = lines.get(i);
            for (int j = 0; j < lineLength; j++) {
                char currentChar = line.charAt(j);
                boolean star = currentChar == '*';
                if (star) {
                    index++;
                    populateMatrixInt(starMatrixInt, index, i, j);
                }
            }
        }
        for (int[] matrix : starMatrixInt) {
            for (int b : matrix) {
//                System.out.print(b ? "*" : " ");
                System.out.print(b);
            }
            System.out.println();
        }

        //* index, numbers
        Map<Integer, List<Integer>> map = new HashMap<>();
        boolean include = false;
        int includeIndex = 0;
        String number = "";
        for (int i = 0; i < linesCount; i++) {
            String line = lines.get(i);
//            List<Integer> numbers = new ArrayList<>();
            for (int j = 0; j < lineLength; j++) {
                char currentChar = line.charAt(j);
//                System.out.print(currentChar);
                boolean digit = Character.isDigit(currentChar);
                if (digit) {
                    number = number + currentChar;
                    int starIndex = starMatrixInt[i][j];
                    if (starIndex != 0) {
                        includeIndex = starIndex;
                    }
                    include = include || starIndex > 0;
                } else {
                    if (include) {
                        int toAdd = Integer.parseInt(number);
//                        numbers.add(toAdd);
                        map.compute(includeIndex, (integer, integers) -> {
                            if (integers == null) {
                                integers = new ArrayList<>();
                            }
                            integers.add(toAdd);
                            return integers;
                        });
                    }
                    include = false;
                    number = "";
                    includeIndex = 0;
                }
            }
            if (include) {
                int toAdd = Integer.parseInt(number);
//                numbers.add(Integer.parseInt(number));
                map.compute(includeIndex, (integer, integers) -> {
                    if (integers == null) {
                        integers = new ArrayList<>();
                    }
                    integers.add(toAdd);
                    return integers;
                });
            }
//            System.out.println(numbers);
//            List<Integer> integers = map.get(includeIndex);
//            if (integers == null) {
//                integers = new ArrayList<>();
//            }
//            integers.addAll(numbers);
//            map.put(includeIndex, integers);

//            System.out.println();
            include = false;
            number = "";
        }
        System.out.println(map);
//        System.out.println(numbers);
//        int sum = numbers.stream().mapToInt(Integer::intValue).sum();
//        System.out.println(sum);
        int sum = map.entrySet().stream()
                .filter(entry -> entry.getValue().size() == 2)
                .mapToInt(entry -> {
                    List<Integer> value = entry.getValue();
                    return value.stream().reduce((integer, integer2) -> integer * integer2).get();
                }).sum();
        System.out.println(sum
        );
    }

    private static void populateMatrix(boolean[][] matrix, int row, int col) {

        int linesCount = matrix.length;
        int lineLength = matrix[0].length;
        //set current as true
        matrix[row][col] = true;
        //set adjusting cells as true
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {
                if (i < 0 || j < 0 || i >= linesCount || j >= lineLength) {
                    continue;
                }
                matrix[i][j] = true;
            }
        }
    }

    private static void populateMatrixInt(int[][] matrix, int index, int row, int col) {

        int linesCount = matrix.length;
        int lineLength = matrix[0].length;
        //set current as true
        matrix[row][col] = index;
        //set adjusting cells as true
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {
                if (i < 0 || j < 0 || i >= linesCount || j >= lineLength) {
                    continue;
                }
                matrix[i][j] = index;
            }
        }
    }

}
