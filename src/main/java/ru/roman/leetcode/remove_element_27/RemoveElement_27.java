package ru.roman.leetcode.remove_element_27;

public class RemoveElement_27 {

    public static void main(String[] args) {
//        int[] nums = new int[] {0,1,2,2,3,0,4,2};
//        int val = 2;
        int[] nums = new int[] {4,4,4};
        int val = 4;
        int k = new RemoveElement_27().removeElement(nums, val);

        System.out.println(k);
        for (int num : nums) {
            System.out.println(num);
        }
    }

//    public int removeElement(int[] nums, int val) {
//        int start = 0;
//        int end = nums.length - 1;
//        int k = 0;
//        for (int i = start; i < end; i++) {
//            if(i == end) break;
//            if (nums[i] == val) {
////                k--;
//                for (int j = end; j > i; j--) {
//                    if (j == i) break;
//                    if (nums[j] != val) {
//                        nums[i] = nums[j];
//                        nums[j] = val;
//                    }
//                }
//            }
//
//        }
//        for (int i = 0; i < nums.length - 1; i++) {
//            if (nums[i] != val) {
//                k++;
//            } else break;
//
//        }
//        return k;
//    }

    public int removeElement(int[] nums, int val) {
        int k = 0;
        int p = nums.length - 1;
        for (int i = 0; i < nums.length; i++) {
            if (i > p) break;
            if (nums[i] == val) {
//                k--;
//                if(i == k) break;
                for (int j = p; j > 0 ; j--) {
                    if (nums[j] != val) {
                        nums[i] = nums[j];
                        nums[j] = val;
                        p--;
                        break;
                    }
                }
            }
        }
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] != val) {
                k++;
            } else break;

        }
        return k;
    }
}
