package ru.roman.leetcode.merged_sorted_array_88;

public class MergedSortedArray_88 {

    public static void main(String[] args) {

        int[] nums1 = new int[]{1, 2, 3, 0, 0, 0};
        int m = 3;
        int[] nums2 = new int[]{2, 5, 6};
        int n = 3;
//        int[] nums1 = new int[]{0};
//        int m = 0;
//        int[] nums2 = new int[]{1};
//        int n = 1;
        new MergedSortedArray_88().merge2(nums1, m, nums2, n);
        for (int i : nums1) {
            System.out.println(i);
        }
    }

    public void merge(int[] nums1, int m, int[] nums2, int n) {

        int i = 0;
        int j = 0;
        int result[] = new int[n + m];
        while (i <= m && j <= n) {
            if (i + j == m + n) {
                break;
            }
            int first = i < m ? nums1[i] : Integer.MAX_VALUE;
            int second = j < n ? nums2[j] : Integer.MAX_VALUE;
            if (first <= second) {
                result[i + j] = first;
                i++;
            } else {
                result[i + j] = second;
                j++;
            }
        }
        for (int k = 0; k < result.length; k++) {
            nums1[k] = result[k];
        }


    }

    public void merge2(int[] nums1, int m, int[] nums2, int n) {

        if(nums1.length == 0) return;
        int i = m - 1;
        int j = n - 1;
        int k = nums1.length - 1;
        while (k >= 0) {
            int first = i >= 0 ? nums1[i] : Integer.MIN_VALUE;
            int second = j >= 0 ? nums2[j] : Integer.MIN_VALUE;
            if(first >= second) {
                nums1[k] = first;
                i--;
            }
            else {
                nums1[k] = second;
                j--;
            }
            k--;
        }
    }
}
