package ru.roman.kakuro;

public record Cell(boolean enabled,
                   int value,
                   int horizontalSum,
                   int horizontalCount,
                   int verticalSum,
                   int verticalCount) {

    public Cell(boolean enabled) {

        this(enabled, 0, 0, 0, 0, 0);
    }
}
