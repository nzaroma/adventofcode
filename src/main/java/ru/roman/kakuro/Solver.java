package ru.roman.kakuro;

import java.util.Set;
import java.util.stream.LongStream;

public class Solver {

    public static void main(String[] args) {

    }

    private Set<Integer> getCombinations(int number, int size) {

        if (size < 2 || size > 9) {
            throw new IllegalArgumentException("Size can not be less than 2 or greater than 9, but was " + size);
        }
        if (size >= number) {
            throw new IllegalArgumentException("Size can not be greater than number");
        }

        long upTo = (long) Math.pow(2, 9) - 1;
        LongStream.rangeClosed(1, upTo)
                .mapToObj(Long::toBinaryString)
                .map(s -> {
                    int lineLength = s.length();
                    int needed = 9 - lineLength;
                    return "0".repeat(needed) + s;
                });
        return null;
    }

}
