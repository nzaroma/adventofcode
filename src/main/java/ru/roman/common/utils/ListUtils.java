package ru.roman.common.utils;

import java.util.ArrayList;
import java.util.List;

public class ListUtils {
    public static List<List<String>> getCombinations(List<String> input) {
        List<List<String>> result = new ArrayList<>();
        combine(input, new ArrayList<>(), result);
        return result;
    }

    private static void combine(List<String> inputList, List<String> intermediateList, List<List<String>> resultList) {
        if (inputList.size() == 1) {
            intermediateList.add(inputList.get(0));
            resultList.add(intermediateList);
            return;
        }
        for (int i = 0; i < inputList.size(); i++) {
            ArrayList<String> newIntermediate = new ArrayList<>(intermediateList);
            ArrayList<String> localList = new ArrayList<>(inputList);
            String removed = localList.remove(i);
            newIntermediate.add(removed);
            combine(localList, newIntermediate, resultList);
        }
    }
}
