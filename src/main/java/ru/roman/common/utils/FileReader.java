package ru.roman.common.utils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

public class FileReader {

    public static List<String> readFile(String fileName) {
        try {
            URL resource = FileReader.class.getClassLoader().getResource(fileName);
            return Files.readAllLines(Path.of(resource.toURI()));
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
