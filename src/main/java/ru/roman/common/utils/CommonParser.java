package ru.roman.common.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface CommonParser<T> {

    default List<T> parseFile(String fileName) {
        List<String> strings = FileReader.readFile(fileName);
        return strings.stream()
                .map(this::parse)
                .toList();
    }

    default T parse(String line) {
        Matcher matcher = Pattern.compile(getPattern()).matcher(line);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("no matches");
        }

        Map<String, Object> map = new LinkedHashMap<>();
        for (Field declaredField : getClazz().getDeclaredFields()) {
            String name = declaredField.getName();
            Class<?> type = declaredField.getType();
            if (type.equals(int.class)) {
                map.put(name, Integer.parseInt(matcher.group(name)));
            } else if (type.equals(String.class)) {
                map.put(name, matcher.group(name));
            } else if (type.equals(double.class)) {
                map.put(name, Double.parseDouble(matcher.group(name)));
            } else if (type.isEnum()) {
                String group = matcher.group(name);
                map.put(name, Enum.valueOf(type.asSubclass(Enum.class), group));
            } else {
                throw new IllegalArgumentException("Not implemented for " + type);
            }
        }
        Object[] args = map.values().toArray();
        Constructor<?> declaredConstructor = getClazz().getDeclaredConstructors()[0];
        T o = null;
        try {
            o = (T) declaredConstructor.newInstance(args);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return o;
    }

    String getPattern();

    Class<T> getClazz();

}

