package ru.roman.year2016.day1;

import java.util.*;

public class Runner {

    public static void main(String[] args) {
        Point point = new Point();
        String input = "R3, L5, R1, R2, L5, R2, R3, L2, L5, R5, L4, L3, R5, L1, R3, R4, R1, L3, R3, L2, L5, L2, R4, R5, R5, L4, L3, L3, R4, R4, R5, L5, L3, R2, R2, L3, L4, L5, R1, R3, L3, R2, L3, R5, L194, L2, L5, R2, R1, R1, L1, L5, L4, R4, R2, R2, L4, L1, R2, R53, R3, L5, R72, R2, L5, R3, L4, R187, L4, L5, L2, R1, R3, R5, L4, L4, R2, R5, L5, L4, L3, R5, L2, R1, R1, R4, L1, R2, L3, R5, L4, R2, L3, R1, L4, R4, L1, L2, R3, L1, L1, R4, R3, L4, R2, R5, L2, L3, L3, L1, R3, R5, R2, R3, R1, R2, L1, L4, L5, L2, R4, R5, L2, R4, R4, L3, R2, R1, L4, R3, L3, L4, L3, L1, R3, L2, R2, L4, L4, L5, R3, R5, R3, L2, R5, L2, L1, L5, L1, R2, R4, L5, R2, L4, L5, L4, L5, L2, L5, L4, R5, R3, R2, R2, L3, R3, L2, L5";
        for (String command : input.split(", ")) {
//            System.out.println(command);
            char c = command.charAt(0);
            if (c == 'R') {
                point.turnRight();
            } else if (c == 'L') {
                point.turnLeft();
            } else {
                throw new IllegalArgumentException("Wrong direction");
            }
            String substring = command.substring(1);
            int distance = Integer.parseInt(substring);
            Point move = point.move(distance);
            if (move != null) {
                System.out.println(move);
                int i = Math.abs(move.getX()) + Math.abs(move.getY());
                System.out.println(i);
                break;
            }



        }

//        System.out.println(point);
//        int i = Math.abs(point.x) + Math.abs(point.y);
//        System.out.println(i);
    }

}

class Point {
    private int x, y;
    private Point direction;
    private List<Point> list = new ArrayList<>();

    public Point() {
        this.direction = new Point(0, 1);
        list.add(new Point(0, 0));
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    Point move(int distance) {
        if (distance < 1) {
            return null;
        }
        for (int i = 1; i <= distance; i++) {
            if (direction.x == 1) x = x + 1;
            else if (direction.x == -1) x = x - 1;
            else if (direction.y == 1) y = y + 1;
            else if (direction.y == -1) y = y - 1;
            Point diffPoint = new Point(x, y);
            if (list.contains(diffPoint)) {
                return diffPoint;
            }
            list.add(diffPoint);

        }
//        if (direction.x == 1) x = x + distance;
//        else if (direction.x == -1) x = x - distance;
//        else if (direction.y == 1) y = y + distance;
//        else if (direction.y == -1) y = y - distance;
        return null;
    }

    void turnLeft() {
        if (direction.x == -1) {
            direction.x = 0;
            direction.y = -1;
        } else if (direction.x == 1) {
            direction.x = 0;
            direction.y = 1;
        } else if (direction.y == 1) {
            direction.x = -1;
            direction.y = 0;
        } else if (direction.y == -1) {
            direction.x = 1;
            direction.y = 0;
        }
    }

    void turnRight() {
        turnLeft();
        turnLeft();
        turnLeft();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Point getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                ", direction=" + direction +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
