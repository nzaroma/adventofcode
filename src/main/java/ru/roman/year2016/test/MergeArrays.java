package ru.roman.year2016.test;

import java.util.HashMap;
import java.util.Map;

public class MergeArrays {
    public static void main(String[] args) {
        String input = "bbtablud";
        int i = new MergeArrays().lengthOfLongestSubstring(input);
        System.out.println(i);
    }

    public int lengthOfLongestSubstring(String input) {
        if (input == null || input.length() == 0) return 0;
        int left = 0, right = 0, length = 0, maxLength = 0;
        Map<Character, Integer> map = new HashMap<>();
        while (right < input.length()) {
            char c = input.charAt(right);
            Integer previous = map.put(c, right);
            right++;
            length++;

            if (previous == null) {
                if(maxLength < length) maxLength = length;
            } else {
                while (left <= previous) {
                    length--;
                    left++;
                }
            }

        }
        return maxLength;
    }
//    012345
//    abcabb
//
//
//    l0001
//    r0123
//    s123

}
