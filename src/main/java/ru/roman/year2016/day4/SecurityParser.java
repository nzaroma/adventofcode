package ru.roman.year2016.day4;

import ru.roman.common.utils.CommonParser;

public class SecurityParser implements CommonParser<Security> {

    @Override
    public String getPattern() {

        return "(?<name>[a-zA-Z-]*)-(?<id>\\d*)\\[(?<checksum>\\w*)\\]";
    }

    @Override
    public Class<Security> getClazz() {

        return Security.class;
    }
}
