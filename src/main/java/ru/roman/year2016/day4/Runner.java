package ru.roman.year2016.day4;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import ru.roman.common.utils.CommonParser;

//@BenchmarkMode
public class Runner {

    public static final int NUMBER_OF_LETTERS = 26;
    public static final int A = 97;
    public static final int Z = 122;
    public static final int SPACE = 32;
    public static final int DASH = 45;

    public static void main(String[] args) {

        CommonParser<Security> parser = new SecurityParser();
        List<Security> securities = parser.parseFile("2016/task4.txt");
        //* task 1 *//
        long sum = securities.stream()
                .parallel()
                .filter(Runner::isRealRoom)
                .mapToLong(Security::id)
                .sum();
        System.out.println(sum);
        //* task 2 *//
        Integer id = securities.stream()
                .parallel()
                .filter(Runner::isNorthPoleStorage)
                .map(Security::id)
                .findAny()
                .orElseThrow();

        System.out.println(id);

    }

    private static boolean isNorthPoleStorage(Security security) {
        return transform(security).startsWith("north");
    }

    private static String transform(Security security) {
        int times = security.id() % NUMBER_OF_LETTERS;
        String decoded = security.name().chars()
                .map(i -> transform(i, times))
                .mapToObj(Character::toString)
                .collect(Collectors.joining());
        return decoded;
    }

    private static int transform(int letter, int times) {
        if (DASH == letter) {
            return times % 2 == 0 ? letter : SPACE;
        }
        int shifted = letter + times;
        if (shifted <= Z) return shifted;
        int diff = shifted - Z - 1;
        return A + diff;
    }

    private static boolean isRealRoom(Security security) {

        String checkSum = Arrays.stream(security.name()
                        .replaceAll("-", "")
                        .split(""))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .sorted(Comparator
                        .comparingLong((Entry<String, Long> entry) -> entry.getValue())
                        .reversed()
                        .thenComparing(Entry::getKey)
                )
                .limit(5)
                .map(Entry::getKey)
                .collect(Collectors.joining());
        return security.checksum().equalsIgnoreCase(checkSum);
    }

}
