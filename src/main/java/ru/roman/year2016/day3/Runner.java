package ru.roman.year2016.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import ru.roman.common.utils.CommonParser;

public class Runner {

    public static void main(String[] args) throws InterruptedException {

        CommonParser<Triangle> parser = new TriangleParser();
        List<Triangle> triangles = parser.parseFile("2016/task3.txt");
        System.out.println(triangles.size());

        Runner runner = new Runner();

        System.out.println("***********");

        runner.runConsequently(triangles);
        runner.runParallel(triangles);

        System.out.print("MultiThreading   ");
        long start = System.nanoTime();
        TriangleSync triangleSync = new TriangleSync(triangles);
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        IntStream.rangeClosed(0, 7)
                .mapToObj(value -> new TriangleParallel(triangleSync))
                .forEach(executorService::submit);

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
        long end = System.nanoTime();
        System.out.println((end - start) / 1000000d);

        List<Triangle> newTriangles = new ArrayList<>();
        for (int i = 0; i < triangles.size(); i=i+3) {
            Triangle a = new Triangle(triangles.get(i).a(), triangles.get(i + 1).a(), triangles.get(i + 2).a());
            Triangle b = new Triangle(triangles.get(i).b(), triangles.get(i + 1).b(), triangles.get(i + 2).b());
            Triangle c = new Triangle(triangles.get(i).c(), triangles.get(i + 1).c(), triangles.get(i + 2).c());
            newTriangles.add(a);
            newTriangles.add(b);
            newTriangles.add(c);
        }
        runner.runParallel(newTriangles);


    }

    public void runParallel(List<Triangle> triangles) {

        System.out.print("Parallel   ");
        long start = System.nanoTime();
        int size = triangles.stream()
                .parallel()
                .map(triangle -> new ArrayList<>(List.of(triangle.a(), triangle.b(), triangle.c())))
                .peek(list -> list.sort(Integer::compareTo))
                .filter(list -> list.get(2) < list.get(0) + list.get(1))
                .toList()
                .size();
        long end = System.nanoTime();
        System.out.print(size + "  ");
        System.out.println((end - start) / 1000000d);
    }

    public void runConsequently(List<Triangle> triangles) {

        System.out.print("Consequently   ");
        long start = System.nanoTime();
        int size = triangles.stream()
                .map(triangle -> new ArrayList<>(List.of(triangle.a(), triangle.b(), triangle.c())))
                .peek(list -> list.sort(Integer::compareTo))
                .filter(list -> list.get(2) < list.get(0) + list.get(1))
                .toList()
                .size();
        long end = System.nanoTime();
        System.out.println((end - start) / 1000000d);
    }

}

class TriangleSync {

    private AtomicInteger result;
    private Queue<Triangle> queue;

    public TriangleSync(List<Triangle> list) {

        result = new AtomicInteger();
        this.queue = new ArrayBlockingQueue<>(list.size());
        queue.addAll(list);
    }

    public AtomicInteger getResult() {

        return result;
    }

    public Queue<Triangle> getQueue() {

        return queue;
    }
}

class TriangleParallel implements Runnable {

    private final TriangleSync syncObject;

    public TriangleParallel(TriangleSync syncObject) {

        this.syncObject = syncObject;
    }

    @Override
    public void run() {

        while (!syncObject.getQueue().isEmpty()) {
            Triangle triangle = syncObject.getQueue().poll();
            if (triangle == null) {
                return;
            }
            List<Integer> list = new ArrayList<>(
                    List.of(triangle.a(), triangle.b(), triangle.c()));
            list.sort(Integer::compareTo);
            if (list.get(2) < list.get(0) + list.get(1)) {
                syncObject.getResult().incrementAndGet();
            }
        }

    }
}
