package ru.roman.year2016.day3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import ru.roman.common.utils.CommonParser;

@Warmup(iterations = 5, time = -1, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 200, time = -1, timeUnit = TimeUnit.SECONDS)
//@Fork(value = 3, jvmArgsAppend = {"-XX:+UseParallelGC", "-Xms1g", "-Xmx1g"})
@Fork(value = 1)
@BenchmarkMode(Mode.SampleTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(org.openjdk.jmh.annotations.Scope.Benchmark)
public class TriangleConsistent {

    private List<Triangle> triangles;

    @Setup
    public void setup() {

        CommonParser<Triangle> parser = new TriangleParser();
        triangles = parser.parseFile("2016/task3.txt");
        System.out.println(triangles.size());
    }

//    public void prepare() {
//
//    }

    @Benchmark
    public int runConsequently() {

//        System.out.print("Consequently   ");
//        long start = System.nanoTime();
        int size = triangles.stream()
                .map(triangle -> new ArrayList<>(List.of(triangle.a(), triangle.b(), triangle.c())))
                .peek(list -> list.sort(Integer::compareTo))
                .filter(list -> list.get(2) < list.get(0) + list.get(1))
                .toList()
                .size();
//        long end = System.nanoTime();
//        System.out.println((end - start) / 1000000d);
        return size;
    }

    @Benchmark
    public int runParallel() {

//        System.out.print("Parallel   ");
//        long start = System.nanoTime();
        int size = triangles.stream()
                .parallel()
                .map(triangle -> new ArrayList<>(List.of(triangle.a(), triangle.b(), triangle.c())))
                .peek(list -> list.sort(Integer::compareTo))
                .filter(list -> list.get(2) < list.get(0) + list.get(1))
                .toList()
                .size();
//        long end = System.nanoTime();
//        System.out.print(size + "  ");
//        System.out.println((end - start) / 1000000d);
        return size;
    }


}

