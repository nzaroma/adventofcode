package ru.roman.year2016.day3;

import ru.roman.common.utils.CommonParser;

public class TriangleParser implements CommonParser<Triangle> {

    @Override
    public String getPattern() {

        return "\\s*(?<a>\\d*)\\s*(?<b>\\d*)\\s*(?<c>\\d*)";
    }

    @Override
    public Class<Triangle> getClazz() {

        return Triangle.class;
    }
}
