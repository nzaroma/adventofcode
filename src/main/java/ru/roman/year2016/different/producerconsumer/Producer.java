package ru.roman.year2016.different.producerconsumer;

public interface Producer {
    void produce() throws InterruptedException;
}
