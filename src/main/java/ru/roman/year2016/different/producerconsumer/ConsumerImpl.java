package ru.roman.year2016.different.producerconsumer;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public class ConsumerImpl implements Consumer, Runnable {

    private BlockingQueue<String> messages;
    private volatile boolean shouldRun;
    private long timeout;

    public ConsumerImpl(BlockingQueue<String> messages) {
        this.messages = messages;
        this.shouldRun = true;
        timeout = new Random().longs(1000, 3000).findAny().orElse(5000);
    }

    @Override
    public void run() {
        System.out.println("Starting consumer " + Thread.currentThread().getName());
        while (shouldRun) {
            try {
                consume();
                System.out.println("Sleep " + timeout + "  " + Thread.currentThread().getName());
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            shouldRun = new Random().nextBoolean();
        }
        System.out.println("Stopping consumer  " + Thread.currentThread().getName());
    }

    @Override
    public void consume() throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + " trying to take message ");
        String message = messages.poll(5, TimeUnit.SECONDS);
        System.out.println(Thread.currentThread().getName() + " took message " + message);
        if (message == null) {
            shouldRun = false;
        }
    }

}
