package ru.roman.year2016.different.producerconsumer;

public interface Consumer {
    void consume() throws InterruptedException;
}
