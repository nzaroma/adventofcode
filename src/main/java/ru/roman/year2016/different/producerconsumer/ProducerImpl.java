package ru.roman.year2016.different.producerconsumer;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class ProducerImpl implements Runnable, Producer {

    private BlockingQueue<String> messages;
    private volatile boolean shouldRun;
    private long timeout;

    public ProducerImpl(BlockingQueue<String> messages) {
        this.messages = messages;
        shouldRun = true;
        timeout = new Random().longs(1000, 3000).findAny().orElse(5000);
    }

    @Override
    public void run() {
        System.out.println("Starting producer");
        while (shouldRun) {
            try {
                produce();
                System.out.println("Sleep " + timeout + "  " + Thread.currentThread().getName());
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            shouldRun = new Random().nextBoolean();
        }
        System.out.println("Stopping producer");
    }

    public void stop() {
        shouldRun = false;
    }

    @Override
    public void produce() throws InterruptedException {
        String message = Thread.currentThread().getName() + System.currentTimeMillis();
        System.out.println("Generating and put " + message + " for " + Thread.currentThread().getName());
        messages.put(message);
    }
}
