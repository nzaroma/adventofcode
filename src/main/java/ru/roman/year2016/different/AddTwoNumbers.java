package ru.roman.year2016.different;

public class AddTwoNumbers {
    public static void main(String[] args) {
        AddTwoNumbers addTwoNumbers = new AddTwoNumbers();
        ListNode first = new ListNode(2, new ListNode(4, new ListNode(3)));
        ListNode second = new ListNode(5, new ListNode(6, new ListNode(4)));
        ListNode listNode = addTwoNumbers.addTwoNumbers(first, second);
        System.out.println(listNode);
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        return sum(l1, l2, 0, new ListNode());
    }

    private ListNode sum(ListNode l1, ListNode l2, int extra, ListNode result) {
        int sum = get(l1) + get(l2) + extra;
        int extraValue = sum > 9 ? 1 : 0;
        if (sum == 0) {
            result.val = 0;
        } else {
            result.val = sum > 9 ? sum - 10 : sum;
        }

        if (extraValue > 0 || getNext(l1) != null || getNext(l2) != null) {
            result.next = sum(getNext(l1), getNext(l2), extraValue, new ListNode());
        }
        return result;
    }

    private int get(ListNode l) {
        if (l == null) {
            return 0;
        }
        return l.val;
    }

    private ListNode getNext(ListNode l) {
        if (l == null) return null;
        return l.next;
    }
}

class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}
