package ru.roman.year2016.different.serialize.pac;

import java.io.Serializable;

public class A implements Serializable {
    private String name;
    private transient int age;

    public A(String name, int age) {

        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "B{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
