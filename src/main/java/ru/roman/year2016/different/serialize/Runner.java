package ru.roman.year2016.different.serialize;

import java.io.*;

public class Runner {
    public static void main(String[] args) {
        A a = new A("Hello", 14);
        FileOutputStream fileOutputStream = null;
        try {
//            fileOutputStream = new FileOutputStream("fileName.txt");
//            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
//            objectOutputStream.writeObject(a);
//            objectOutputStream.flush();

            FileInputStream fileInputStream = new FileInputStream("fileName.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            A a2 = (A) objectInputStream.readObject();
            System.out.println(a2);
            objectInputStream.close();
            fileInputStream.close();
//            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

    }
}

class A implements Externalizable {

    private static final long serialVersionUID = 1L;
    private String name2;
    private transient int age;
    private static int number = 16;

    public A() {

    }
    public A(String name, int age) {
        this.name2 = name;
        this.age = age;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static int getNumber() {
        return number;
    }

    public static void setNumber(int number) {
        ru.roman.year2016.different.serialize.A.number = number;
    }

    @Override
    public String toString() {
        return "A{" +
                "name='" + name2 + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(name2);
        out.writeInt(10);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        name2 = (String) in.readObject();
        age = in.readInt();
    }
}