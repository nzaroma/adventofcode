package ru.roman.year2016.day5;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Runner {
    private MessageDigest md;

    public Runner() {
        try {
            this.md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        //branch 1 changes
        //new change222
        //main changes
        String input = "abbhdwsy";
        /* part 1 */
        String s = new Runner().find(input);

        System.out.println(s);
    }

    public String find(String input) {
        return IntStream.iterate(0, value -> value + 1)
                .mapToObj(i -> input + i)
                .map(this::calculateHash)
                .filter(this::hasLeadZero)
                .map(this::getPasswordLetter)
                .limit(8)
                .collect(Collectors.joining());
    }

    public String calculateHash(String input) {
        md.update(input.getBytes());
        byte[] digest = md.digest();
        String myHash = DatatypeConverter
                .printHexBinary(digest).toUpperCase();
        return myHash;
    }

    public boolean hasLeadZero(String hash) {
        return hash.startsWith("00000");
    }

    public String getPasswordLetter(String hash) {
        return Character.toString(hash.charAt(5));
    }

}
