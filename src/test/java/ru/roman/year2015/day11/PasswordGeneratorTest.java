package ru.roman.year2015.day11;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class PasswordGeneratorTest {


    @ParameterizedTest
    @CsvSource({
            "a, b",
            "az, ba",
    })
    void generateNextPassword(String input, String expected) {
        PasswordGenerator passwordGenerator = new PasswordGenerator(new WordFilter());
        String actual = passwordGenerator.generateNextPassword(input);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @CsvSource({
            "abcdefgh, abcdffaa",
            "ghijklmn, ghjaabcc",
    })
    void generateNextValidPassword(String input, String expected) {
        PasswordGenerator passwordGenerator = new PasswordGenerator(new WordFilter());
        String result = passwordGenerator.generateNextValidPassword(input);
        Assertions.assertEquals(expected, result);
    }
}