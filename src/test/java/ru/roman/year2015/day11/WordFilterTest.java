package ru.roman.year2015.day11;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class WordFilterTest {


    @ParameterizedTest
    @CsvSource({
            "hijklmmn, false",
            "abbceffg, false",
            "abbcegjk, false",
            "abcdffaa, true",
            "ghjaabcc, true",
            "aabaa, false"
    })
    void isValid(String input, boolean valid) {
        WordFilter filter = new WordFilter();
        boolean result = filter.isValid(input);
        Assertions.assertEquals(valid, result);
    }
}