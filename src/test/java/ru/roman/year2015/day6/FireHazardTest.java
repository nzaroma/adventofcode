package ru.roman.year2015.day6;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class FireHazardTest {

    @Test
    void calculateCells() {

        FireHazard fireHazard = new FireHazard();
        Cell[][] cells = fireHazard.init();
        long actual = fireHazard.calculateLitCells(cells);
        Assertions.assertEquals(0, actual);
    }

    @Test
    void calculateCells2() {
        FireHazard fireHazard = new FireHazard();
        Cell[][] cells = fireHazard.init();
        cells[0][0].setState(1);
        long actual = fireHazard.calculateLitCells(cells);
        Assertions.assertEquals(1, actual);
    }

    @ParameterizedTest
    @CsvSource({
            "'turn on 0,0 through 999,999', 1000000",
            "'toggle 0,0 through 999,0', 1000",
            "'turn off 499,499 through 500,500', 0",

    })
    void testApplyCommandBasicStrategy(String input, int expected) {
        FireHazard fireHazard = new FireHazard();
        Cell[][] cells = fireHazard.init();
        Parser parser = new Parser();
        Command command = parser.parse(input);
        fireHazard.applyCommand(command, cells, new BasicLitStrategy());
        long actual = fireHazard.calculateLitCells(cells);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @CsvSource({
            "'turn on 0,0 through 0,0', 1",
            "'toggle 0,0 through 999,999', 2000000",

    })
    void testApplyCommandBrightenStrategy(String input, int expected) {
        FireHazard fireHazard = new FireHazard();
        Cell[][] cells = fireHazard.init();
        Parser parser = new Parser();
        Command command = parser.parse(input);
        fireHazard.applyCommand(command, cells, new BrightenStrategy());
        long actual = fireHazard.calculateLitCells(cells);
        Assertions.assertEquals(expected, actual);
    }

}