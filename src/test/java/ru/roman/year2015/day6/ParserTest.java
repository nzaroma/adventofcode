package ru.roman.year2015.day6;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {

    @Test
    void testCommandCreation() {
        String input = "turn on 0,0 through 999,999";
        Command expected = new Command(Action.TURN_ON, 0, 0, 999, 999);
        Parser parser = new Parser();
        Command actual = parser.parse(input);
        Assertions.assertEquals(expected, actual);

    }

}