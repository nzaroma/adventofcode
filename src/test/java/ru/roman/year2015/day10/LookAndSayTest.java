package ru.roman.year2015.day10;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class LookAndSayTest {

    @Test
    void transformNTimes() {
        LookAndSay lookAndSay = new LookAndSay();
        int i = lookAndSay.transformNTimes("1", 5);
        Assertions.assertEquals(6, i);
    }

    @ParameterizedTest
    @CsvSource({
            "1, 11",
            "11, 21",
            "21, 1211",
            "1211, 111221",
            "111221, 312211",
    })
    void transform(String input, String output) {
        LookAndSay lookAndSay = new LookAndSay();
        String result = lookAndSay.transform(input);
        Assertions.assertEquals(output, result);
    }
}