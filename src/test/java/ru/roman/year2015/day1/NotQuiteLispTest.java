package ru.roman.year2015.day1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class NotQuiteLispTest {

    @Test
    void shouldAddFloor() {
        NotQuiteLisp service = new NotQuiteLisp();
        Assertions.assertEquals(1, service.calculateFloor("("));
    }

    @Test
    void shouldRemoveFloor() {
        NotQuiteLisp service = new NotQuiteLisp();
        Assertions.assertEquals(-1, service.calculateFloor(")"));
    }

    /**
     *   (()) and ()() both result in floor 0.
     *   ((( and (()(()( both result in floor 3.
     *   ))((((( also results in floor 3.
     *   ()) and ))( both result in floor -1 (the first basement level).
     *   ))) and )())()) both result in floor -3.
     *   To what floor do the instructions take Santa?
     *
     */
    @ParameterizedTest
    @CsvSource({
            "(()), 0",
            "()(), 0",
            "))(((((, 3"
    })
    void shouldCalculate(String input, int expected) {
        NotQuiteLisp service = new NotQuiteLisp();
        Assertions.assertEquals(expected, service.calculateFloor(input));
    }

    @ParameterizedTest
    @CsvSource({
            "), 1",
            "()()), 5"
    })
    void shouldEnterBasement(String input, int expected) {
        NotQuiteLisp service = new NotQuiteLisp();
        Assertions.assertEquals(expected, service.getBasementPosition(input));
    }
}
