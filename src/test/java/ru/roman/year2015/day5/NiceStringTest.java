package ru.roman.year2015.day5;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.List;

class NiceStringTest {

    @ParameterizedTest
    @CsvSource({
            "ugknbfddgicrmopn, true",
            "jchzalrnumimnmhp, false",
            "haegwjzuvuyypxyu, false",
            "dvszwmarrgswjxmb, false",
            "aaa, true"
    })
    void testSimpleStrategy(String input, boolean expected) {
        StringStrategy service = new EasyStrategy();
        boolean actual = service.apply(List.of(input)) == 1;
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @CsvSource({
            "qjhvhtzxzqqjkmpb, true",
            "uurcxstgmygtbstg, false",
            "ieodomkazucvgmuy, false",
            "xxyxx, true"
    })
    void testAdvancedStrategy(String input, boolean expected) {
        StringStrategy service = new AdvancedStrategy();
        boolean actual = service.apply(List.of(input)) == 1;
        Assertions.assertEquals(expected, actual);
    }
}