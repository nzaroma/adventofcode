package ru.roman.year2015.day15;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WithoutCaloriesCalculatingStrategyTest {

    @Test
    void testApplyMethod() {
        /**
         * Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
         * Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3
         */
        Ingredient first = new Ingredient("Butterscotch", -1, -2, 6, 3, 8);
        Ingredient second = new Ingredient("Cinnamon", 2, 3, -2, -1, 3);
        WithoutCaloriesCalculatingStrategy strategy = new WithoutCaloriesCalculatingStrategy(List.of(first, second));
        int result = strategy.apply();
        assertEquals(62842880, result);

    }

}