package ru.roman.year2015.day15;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class IncrementorTest {

    @Test
    void testIncrementor2() {
        Incrementor incrementor = new Incrementor(2);
        while (incrementor.hasNext()) {
            List<Integer> next = incrementor.next();
//            System.out.println(next);
        }
    }

    @Test
    void testIncrementor3() {
        Incrementor incrementor = new Incrementor(3);
        while (incrementor.hasNext()) {
            List<Integer> next = incrementor.next();
//            System.out.println(next);
        }
    }

    @Test
    void testIncrementor4() {
        Incrementor incrementor = new Incrementor(4);
        while (incrementor.hasNext()) {
            List<Integer> next = incrementor.next();
//            System.out.println(next);
        }
    }

}