package ru.roman.year2015.day18;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    @Test
    void calculate() {
        int[][] input = new int[][] {
                new int[] {0,1,0,1,0,1},
                new int[] {0,0,0,1,1,0},
                new int[] {1,0,0,0,0,1},
                new int[] {0,0,1,0,0,0},
                new int[] {1,0,1,0,0,1},
                new int[] {1,1,1,1,0,0},
        };
        Calculator calculator = new Calculator(input);
        int i = calculator.calculateLights(4, false);
        Assertions.assertEquals(4, i);

    }

    @Test
    void calculateNew() {
        int[][] input = new int[][] {
                new int[] {0,1,0,1,0,1},
                new int[] {0,0,0,1,1,0},
                new int[] {1,0,0,0,0,1},
                new int[] {0,0,1,0,0,0},
                new int[] {1,0,1,0,0,1},
                new int[] {1,1,1,1,0,0},
        };
        Calculator calculator = new Calculator(input);
        int i = calculator.calculateLights(5, true);
        Assertions.assertEquals(17, i);

    }

}