package ru.roman.year2015.day8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ParserTest {
    @ParameterizedTest
    @CsvSource({
            "\"\", 2, 0, 6",
            "\"abc\", 5, 3, 9",
            "\"aaa\\\"aaa\", 10, 7, 16",
            "\"\\x27\", 6, 1, 11",
    })
    void shouldParse(String inputLine, int total, int real, int encoded) {
        Parser parser = new Parser();
        Result result = parser.parseLine(inputLine);
        Assertions.assertEquals(total, result.total());
        Assertions.assertEquals(real, result.real());
        Assertions.assertEquals(encoded, result.encoded());
    }

}