package ru.roman.year2015.concurrent;

import java.util.LinkedList;
import java.util.Queue;

public class DataQueue {
    private final Queue<String> queue = new LinkedList<>();
    private final int maxSize;
    private final Object emptyLock = new Object();
    private final Object fullLock = new Object();

    public DataQueue(int maxSize) {
        this.maxSize = maxSize;
    }

    private boolean isEmpty() {
        return queue.isEmpty();
    }

    private boolean isFull() {
        return queue.size() == maxSize;
    }

    public void produce(String element) {
        synchronized (queue) {
            while (isFull()) {
                System.out.println("full!");
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            queue.add(element);
            queue.notifyAll();
        }
    }

    public String consume() {
        String remove;
        synchronized (queue) {
            while (isEmpty()) {
                System.out.println("empty!");
                try {
                    queue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            remove = queue.remove();
            queue.notifyAll();
        }
        return remove;
    }
}
