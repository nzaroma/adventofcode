package ru.roman.year2015.concurrent;

import java.util.concurrent.Semaphore;

public class CustomSemaphoreTester {

    public static void main(String[] args) {
        CustomSemaphore semaphore = new CustomSemaphore(2);
        Runnable runnable = () -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Starting " + threadName);
            semaphore.acquire();
            System.out.println("Acquired lock for thread " + threadName);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Releasing lock for thread " + threadName);
            semaphore.release();
            System.out.println("Finishing work for thread " + threadName);
        };
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        Thread thread3 = new Thread(runnable);
        Thread thread4 = new Thread(runnable);
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        System.out.println("Main done");


    }






    static class CustomSemaphore {
        private final int limit;
        private int current;
        private final Object lock = new Object();

        public CustomSemaphore(int limit) {
            this.limit = limit;
        }

        public void acquire() {
            synchronized (lock) {
                if (current == limit) {
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                current++;
            }
        }

        public void release() {
            synchronized (lock) {
                if (current == limit) {
                    lock.notifyAll();
                }
                current--;
            }
        }
    }

}
