package ru.roman.year2015.concurrent;

import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ProducerConsumerV2 {
    public static void main(String[] args) throws InterruptedException {
        DataQueue2 dataQueue2 = new DataQueue2(3);
        Runnable producer1 = () -> {
            for (int i = 0; i < 10; i++) {
                String uuid = UUID.randomUUID().toString();
                dataQueue2.produce(uuid);
                System.out.println(Thread.currentThread().getName() + " produced " + uuid);
            }
        };

        Runnable consumer1 = () -> {
            while (true) {
                String release = dataQueue2.release();
                System.out.println(Thread.currentThread().getName() + " consumed " + release);
            }
        };

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        executorService.submit(consumer1);
        executorService.submit(producer1);
        executorService.submit(producer1);
        executorService.submit(producer1);
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
    }
}

class DataQueue2 {
    private final int limit;
    private final Queue<String> queue;
    private final ReentrantLock lock = new ReentrantLock();

    private final Condition emptyCondition = lock.newCondition();
    private final Condition fullCondition = lock.newCondition();

    public DataQueue2(int limit) {
        this.limit = limit;
        queue = new LinkedList<>();
    }

    public void produce(String element) {
        try {
            lock.lock();
            if (limit == queue.size()) {
                System.out.println("full");
                fullCondition.await();
            }
            queue.add(element);
            emptyCondition.signal();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public String release() {
        String result = null;
        try {
            lock.lock();
            if (queue.isEmpty()) {
                System.out.println("empty");
                emptyCondition.await();
            }
            result = queue.remove();
            fullCondition.signal();
            return result;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return result;
    }
}
