package ru.roman.year2015.concurrent;

public class ProducerConsumerRunner {
    public static void main(String[] args) throws InterruptedException {
        DataQueue dataQueue = new DataQueue(3);
        ProducerImpl producer = new ProducerImpl(dataQueue);
        Thread producerThread = new Thread(producer);
        Thread producerThread2 = new Thread(producer);
        Thread producerThread3 = new Thread(producer);
        Thread producerThread4 = new Thread(producer);
        Thread consumerThread = new Thread(new ConsumerImpl(dataQueue));
        Thread consumerThread2 = new Thread(new ConsumerImpl(dataQueue));
        System.out.println("Starting program");
        consumerThread.start();
//        consumer2.start();
        Thread.sleep(10);
        producerThread.start();
        producerThread2.start();
        producerThread3.start();
        producerThread4.start();
        Thread.sleep(50);
        producer.stop();
        Thread.sleep(10);

        System.out.println("Exiting main");
    }
}
