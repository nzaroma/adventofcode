package ru.roman.year2015.concurrent;

import java.util.UUID;
import java.util.concurrent.Exchanger;

public class CustomExchangerTester {
    public static void main(String[] args) {
        Exchanger<String> exchanger = new Exchanger<>();
//        CustomExchanger<String> exchanger = new CustomExchanger<>();
        Runnable runnable = () -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Starting " + threadName);
            String data = UUID.randomUUID().toString();
            System.out.println(threadName + " sending:  " + data);
            String result = null;
            try {
                result = exchanger.exchange(data);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(threadName + " receiving   " + result);
        };
        Thread thread1 = new Thread(runnable);
        Thread thread2 = new Thread(runnable);
        Thread thread3 = new Thread(runnable);
        Thread thread4 = new Thread(runnable);
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
    }
}
