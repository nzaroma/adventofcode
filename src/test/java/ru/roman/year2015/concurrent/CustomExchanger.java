package ru.roman.year2015.concurrent;

public class CustomExchanger<E> {
    private final Object lock = new Object();
    private int current;
    private E first;
    private E second;
    private boolean firstReady=true;
    private boolean secondReady=true;
    private boolean exchangeReady;

    public E exchange(E input) {
        synchronized (lock) {
            while (current == 2) {
                System.out.println("inside " + Thread.currentThread().getName());
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (current == 0) {
                first = input;
                current++;
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                current = 0;
                lock.notifyAll();
                return second;
            } else if (current == 1) {
                current++;
                second = input;
                lock.notifyAll();
                return first;
            } else {
                System.out.println("Current = " + current);
                throw new IllegalArgumentException();
            }
        }
    }
}
