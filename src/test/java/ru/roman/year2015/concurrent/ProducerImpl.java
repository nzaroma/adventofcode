package ru.roman.year2015.concurrent;

import java.util.UUID;

public class ProducerImpl implements Runnable{

    private boolean stop;
    private DataQueue dataQueue;

    public void stop() {
        stop = true;
    }

    public ProducerImpl(DataQueue dataQueue) {
        this.dataQueue = dataQueue;
    }

    @Override
    public void run() {
        System.out.println("starting producer: " + Thread.currentThread().getName());
        produce();
        System.out.println("stoping producer: " + Thread.currentThread().getName());
    }

    private void produce() {
        while (!stop) {
            String s = UUID.randomUUID().toString();
            System.out.println("Generated message: " + s);
            dataQueue.produce(s);
        }
    }
}
