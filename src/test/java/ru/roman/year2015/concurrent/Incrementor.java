package ru.roman.year2015.concurrent;

import java.util.concurrent.atomic.AtomicInteger;

public class Incrementor implements Runnable {

    private AtomicInteger ai = new AtomicInteger();
    private volatile boolean done = false;

    public boolean isDone() {
        return done;
    }

    public AtomicInteger getAi() {
        return ai;
    }

    public static void main(String[] args) throws InterruptedException {
        Incrementor incrementor = new Incrementor();
        Thread thread1 = new Thread(incrementor);
        Runnable runnable = () -> {
            while (!incrementor.isDone()) {
                Thread.yield();
            }
            System.out.println("In 2: " + incrementor.getAi().get());
        };
        Thread thread2 = new Thread(runnable);
        thread1.start();
        thread2.start();
    }

    @Override
    public void run() {
        for (int j = 0; j < 1_000_000; j++) {
            ai.incrementAndGet();
        }
        done = true;
    }
}
