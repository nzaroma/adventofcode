package ru.roman.year2015.concurrent;

public class ConsumerImpl implements Runnable{

    private boolean stop;
    private DataQueue dataQueue;

    public ConsumerImpl(DataQueue dataQueue) {
        this.dataQueue = dataQueue;
    }

    @Override
    public void run() {
        System.out.println("starting consumer: " + Thread.currentThread().getName());
        consume();
        System.out.println("stoping consumer: " + Thread.currentThread().getName());
    }

    private void consume() {
        System.out.println("Waiting for a message");
        while (!stop) {

            String consume = dataQueue.consume();
            System.out.println("Consumed message: " + consume);
        }
    }

    public void stop() {
        stop = true;
    }

}
