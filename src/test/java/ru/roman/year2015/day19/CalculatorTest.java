package ru.roman.year2015.day19;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    @Test
    void testCalc() {
        Parser parser = new Parser();
        List<Transition> transitions = parser.parseFile("task19_test.txt");
        Calculator calculator = new Calculator();
        int hoh = calculator.getCombinationCount("HOH", transitions);
        assertEquals(4, hoh);
    }
    @Test
    void testCalc2() {
        Parser parser = new Parser();
        List<Transition> transitions = List.of(new Transition("H", "OO"));
        Calculator calculator = new Calculator();
        int hoh = calculator.getCombinationCount("H2O", transitions);
        assertEquals(1, hoh);
    }


    @Test
    void testVar1() {
        String input = "CRnSiRnCaPTiMgYCaPTiRnFArSiThFArCaSiThSiThPBCaCaSiRnSiRnTiTiMgArPBCaPMgYPTiRnFArFArCaSiRnBPMgArPRnCaPTiRnFArCaSiThCaCaFArPBCaCaPTiTiRnFArCaSiRnSiAlYSiThRnFArArCaSiRnBFArCaCaSiRnSiThCaCaCaFYCaPTiBCaSiThCaSiThPMgArSiRnCaPBFYCaCaFArCaCaCaCaSiThCaSiRnPRnFArPBSiThPRnFArSiRnMgArCaFYFArCaSiRnSiAlArTiTiTiTiTiTiTiRnPMgArPTiTiTiBSiRnSiAlArTiTiRnPMgArCaFYBPBPTiRnSiRnMgArSiThCaFArCaSiThFArPRnFArCaSiRnTiBSiThSiRnSiAlYCaFArPRnFArSiThCaFArCaCaSiThCaCaCaSiRnPRnCaFArFYPMgArCaPBCaPBSiRnFYPBCaFArCaSiAl";
        Parser parser = new Parser();
        List<Transition> transitions = parser.parseFile("task19.txt");
        Calculator calculator = new Calculator();
        int hoh = calculator.getCombinationCount(input, transitions);
        System.out.println(hoh);
    }

}