package ru.roman.year2015.day19;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {

    @Test
    void parseFile() {
        Parser parser = new Parser();
        List<Transition> transitions = parser.parseFile("task19_test.txt");
        Assertions.assertEquals(3, transitions.size());
    }

}