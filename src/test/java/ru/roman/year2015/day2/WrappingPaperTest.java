package ru.roman.year2015.day2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class WrappingPaperTest {

    @ParameterizedTest
    @MethodSource("arrayProvider")
    void calculateOnePresent(int[] input, int expected) {
        WrappingPaper service = new WrappingPaper();
        Assertions.assertEquals(expected, service.calculateOnePresent(input));
    }

    static Stream<Arguments> arrayProvider() {
        return Stream.of(
                Arguments.of((Object) new int[]{2, 3, 4}, 58, 34),
                Arguments.of((Object) new int[]{4, 3, 2}, 58, 34),
                Arguments.of((Object) new int[]{1, 1, 10}, 43, 14)
        );
    }

    @Test
    void calculateListOfPresents() {
//        List<int[]> list = List.of(new int[]{2, 3, 4}, new int[]{1, 1, 10});
        List<String> list = List.of("2x3x4", "1x1x10");
        WrappingPaper service = new WrappingPaper();
        Assertions.assertEquals(58+43, service.calculateTotal(list));
    }

    @ParameterizedTest
    @MethodSource("arrayProvider")
    void calculateOneRibbon(int[] input, int expectedS, int expectedRibbon) {
        WrappingPaper service = new WrappingPaper();
        Assertions.assertEquals(expectedRibbon, service.calculateOneRibbon(input));
    }

    @Test
    void calculateRibbonTotal() {
        List<String> list = List.of("2x3x4", "1x1x10");
        WrappingPaper service = new WrappingPaper();
        Assertions.assertEquals(34+14, service.calculateRibbonTotal(list));
    }

}