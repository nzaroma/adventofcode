package ru.roman.year2015.day4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;

class BitcoinMinerTest {

    @Disabled("obsolete")
    @ParameterizedTest
    @CsvSource({
            "abcdef, 609043, 5",
            "pqrstuv, 1048970, 5"
    })
    void getMinimumNuber(String input, int expected, int zeroCount) throws NoSuchAlgorithmException {
        BitcoinMiner service = new BitcoinMiner();
        int result = service.getMinimumNuber(input, zeroCount);
        Assertions.assertEquals(expected, result);
    }

}