package ru.roman.year2015.day7;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {

    @Test
    void testDirectInstruction() {
        Parser parser = new Parser();
        Instruction instruction = parser.parse("123 -> x");
        Assertions.assertTrue(instruction instanceof DirectInstruction);
        Assertions.assertEquals(123, instruction.getSource().value());
        Assertions.assertEquals(null, instruction.getSource().name());
        Assertions.assertEquals(-1, instruction.getOutput().value());
        Assertions.assertEquals("x", instruction.getOutput().name());
    }

    @Test
    void testNotInstruction() {
        Parser parser = new Parser();
        Instruction instruction = parser.parse("NOT x -> h");
        Assertions.assertTrue(instruction instanceof NotInstruction);
        NotInstruction notInstruction = (NotInstruction) instruction;
        Assertions.assertEquals(-1, instruction.getSource().value());
        Assertions.assertEquals("x", instruction.getSource().name());
        Assertions.assertEquals(-1, instruction.getOutput().value());
        Assertions.assertEquals("h", instruction.getOutput().name());
    }

    @Test
    void testAndInstruction() {
        Parser parser = new Parser();
        Instruction instruction = parser.parse("x AND y -> d");
        Assertions.assertTrue(instruction instanceof AndInstruction);
        AndInstruction andInstruction = (AndInstruction) instruction;
        Assertions.assertEquals(-1, instruction.getSource().value());
        Assertions.assertEquals("x", instruction.getSource().name());
        Assertions.assertEquals(-1, instruction.getOutput().value());
        Assertions.assertEquals("d", instruction.getOutput().name());
        Assertions.assertEquals(-1, andInstruction.getSecondSource().value());
        Assertions.assertEquals("y", andInstruction.getSecondSource().name());
    }

    @Test
    void testOrInstruction() {
        Parser parser = new Parser();
        Instruction instruction = parser.parse("x OR y -> d");
        Assertions.assertTrue(instruction instanceof OrInstruction);
        OrInstruction orInstruction = (OrInstruction) instruction;
        Assertions.assertEquals(-1, instruction.getSource().value());
        Assertions.assertEquals("x", instruction.getSource().name());
        Assertions.assertEquals(-1, instruction.getOutput().value());
        Assertions.assertEquals("d", instruction.getOutput().name());
        Assertions.assertEquals(-1, orInstruction.getSecondSource().value());
        Assertions.assertEquals("y", orInstruction.getSecondSource().name());
    }

    @Test
    void testLshiftInstruction() {
        Parser parser = new Parser();
        Instruction instruction = parser.parse("x LSHIFT 2 -> f");
        Assertions.assertTrue(instruction instanceof LShiftInstruction);
        LShiftInstruction orInstruction = (LShiftInstruction) instruction;
        Assertions.assertEquals(-1, instruction.getSource().value());
        Assertions.assertEquals("x", instruction.getSource().name());
        Assertions.assertEquals(-1, instruction.getOutput().value());
        Assertions.assertEquals("f", instruction.getOutput().name());
        Assertions.assertEquals(2, orInstruction.getShift());
    }

    @Test
    void testRshiftInstruction() {
        Parser parser = new Parser();
        Instruction instruction = parser.parse("x RSHIFT 2 -> f");
        Assertions.assertTrue(instruction instanceof RShiftInstruction);
        RShiftInstruction orInstruction = (RShiftInstruction) instruction;
        Assertions.assertEquals(-1, instruction.getSource().value());
        Assertions.assertEquals("x", instruction.getSource().name());
        Assertions.assertEquals(-1, instruction.getOutput().value());
        Assertions.assertEquals("f", instruction.getOutput().name());
        Assertions.assertEquals(2, orInstruction.getShift());
    }
}