package ru.roman.year2015.day7;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    /**
     * 123 -> x
     * 456 -> y
     * x AND y -> d
     */
    @Test
    void shouldCalculateAnd() {
        Wire x = new Wire("x", 123);
        Wire y = new Wire("y", 456);
        Wire d = new Wire("d", -1);
        AndInstruction andInstruction = new AndInstruction(x, y, d);
        Calculator calculator = new Calculator();
        calculator.calculate(andInstruction);
        Assertions.assertEquals(72, andInstruction.getOutput().value());
    }

    /**
     * 123 -> x
     * 456 -> y
     * x OR y -> e
     */
    @Test
    void shouldCalculateOr() {
        Wire x = new Wire("x", 123);
        Wire y = new Wire("y", 456);
        Wire e = new Wire("e", -1);
        OrInstruction orInstruction = new OrInstruction(x, y, e);
        Calculator calculator = new Calculator();
        calculator.calculate(orInstruction);
        Assertions.assertEquals(507, orInstruction.getOutput().value());
    }

    /**
     * 123 -> x
     * x LSHIFT 2 -> f
     */
    @Test
    void shouldCalculateLshift() {
        Wire x = new Wire("x", 123);
        Wire e = new Wire("f", -1);
        LShiftInstruction instruction = new LShiftInstruction(x, e, 2);
        Calculator calculator = new Calculator();
        calculator.calculate(instruction);
        Assertions.assertEquals(492, instruction.getOutput().value());
    }

    /**
     * 456 -> y
     * x RSHIFT 2 -> g
     */
    @Test
    void shouldCalculateRshift() {
        Wire y = new Wire("y", 456);
        Wire e = new Wire("g", -1);
        RShiftInstruction instruction = new RShiftInstruction(y, e, 2);
        Calculator calculator = new Calculator();
        calculator.calculate(instruction);
        Assertions.assertEquals(114, instruction.getOutput().value());
    }

    /**
     * 456 -> y
     * NOT y -> i
     */
    @Test
    void shouldCalculateNot() {
        Wire y = new Wire("y", 456);
        Wire i = new Wire("i", -1);
        NotInstruction instruction = new NotInstruction(y, i);
        Calculator calculator = new Calculator();
        calculator.calculate(instruction);
        Assertions.assertEquals(65079, instruction.getOutput().value());
    }

    @Test
    void shouldCalculateCircuit() {
        String input = """
                123 -> x
                456 -> y
                x AND y -> d
                x OR y -> e
                x LSHIFT 2 -> f
                y RSHIFT 2 -> g
                NOT x -> h
                NOT y -> i""";
        Parser parser = new Parser();
        List<Instruction> instructions = input.lines().map(parser::parse).toList();
        Calculator calculator = new Calculator();
        int result = calculator.calculateSignal(instructions, "i");
        Assertions.assertEquals(65079, result);
    }
}