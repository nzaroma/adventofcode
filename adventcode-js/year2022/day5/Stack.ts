// var fs = require('fs');
//npm install -D @types/node
//PS C:\personal\java\adventcode\adventcode-js> .\node_modules\.bin\tsc.cmd .\year2022\day5\Stack.ts
//PS C:\personal\java\adventcode\adventcode-js> node .\year2022\day5\Stack.js

import * as fs from 'fs';


class Stack {
  readonly items: string[]
  constructor(items:string[]) {
    this.items = items;
  }
  peek(): string {
    return this.items[this.items.length - 1];
  }
  pop(): string {
    return this.items.pop();
  }
  push(item: string): string {
    this.items.push(item);
    return item;
  }

}
class Ship {
  readonly items: Stack[]
  constructor(input:string[][]) {
    this.items = [];
    for (const element of input) {
      this.items.push(new Stack(element));
    }
  }
  move(count, source, target): void {
    const sourceStack = this.items[source - 1];
    const targetStack = this.items[target - 1];
    for (let i = 0; i < count; i++) {
      const element = sourceStack.pop();
      targetStack.push(element)
    }
  }

  move9001(count, source, target): void {
    const sourceStack = this.items[source - 1];
    const targetStack = this.items[target - 1];
    const tempStack = new Stack([]);
    for (let i = 0; i < count; i++) {
      tempStack.push(sourceStack.pop())
    }
    for (let i = 0; i < count; i++) {
      targetStack.push(tempStack.pop())
    }
  }

  top(): string {
    return this.items.map(value => value.peek()).join('');
  }
}

/*
const ship = new Ship([
    ['Z', 'N'],
    ['M', 'C', 'D'],
    ['P']
])

console.dir(ship, { depth: null });
//     [D]
// [N] [C]
// [Z] [M] [P]
//  1   2   3

// move 1 from 2 to 1
// [D]
// [N] [C]
// [Z] [M] [P]
//  1   2   3
ship.move(1, 2, 1)

// move 3 from 1 to 3
ship.move(3, 1, 3)
// move 2 from 2 to 1
ship.move(2, 2, 1)
// move 1 from 1 to 2
ship.move(1, 1, 2)
console.dir(ship, { depth: null });
console.dir(ship.top())
*/
//         [Q] [B]         [H]
//     [F] [W] [D] [Q]     [S]
//     [D] [C] [N] [S] [G] [F]
//     [R] [D] [L] [C] [N] [Q]     [R]
// [V] [W] [L] [M] [P] [S] [M]     [M]
// [J] [B] [F] [P] [B] [B] [P] [F] [F]
// [B] [V] [G] [J] [N] [D] [B] [L] [V]
// [D] [P] [R] [W] [H] [R] [Z] [W] [S]
// 1   2   3   4   5   6   7   8   9
const ship = new Ship([
    ['D', 'B', 'J', 'V'],
    ['P', 'V', 'B', 'W', 'R', 'D', 'F'],
    ['R', 'G', 'F', 'L', 'D', 'C', 'W', 'Q'],
    ['W', 'J', 'P', 'M', 'L', 'N', 'D', 'B'],
    ['H', 'N', 'B', 'P', 'C', 'S', 'Q'],
    ['R', 'D', 'B', 'S', 'N', 'G'],
    ['Z', 'B', 'P', 'M', 'Q', 'F', 'S', 'H'],
    ['W', 'L', 'F'],
    ['S', 'V', 'F', 'M', 'R']
])

// function


// Use fs.readFile() method to read the file
fs.readFile('./../src/main/resources/2022/task5.txt', 'utf8', function(err, data){
  const result = data.split(/\r?\n/).filter(element => element).map(e => e.split(' '))
  // console.log(result)
  result.forEach(element => ship.move9001(element[1], element[3], element[5]))
  console.dir(ship)

  console.log(ship.top())
});

