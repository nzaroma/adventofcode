import * as fs from 'fs';

class Node {
  name: string
  size: number
  parent: Node
  children: Node[]
  folder: boolean

  constructor(name: string, size: number, folder: boolean) {
    this.name = name;
    this.size = size;
    this.children = [];
    this.folder = folder;
  }

  addChild(node: Node) {
    this.children.push(node)
  }

  findChild(name: string):Node {
    return this.children.find(e => e.name === name)
  }

  updateSize(size: number): void {
    this.size += size;
    this.parent?.updateSize(size);
  }
}

const content = fs.readFileSync('./../src/main/resources/2022/task6.txt', 'utf8');
let strings = content.split(/\r?\n/);
let currentNode;
let rootNode;
for (const line of strings) {
  console.log('Line: ' + line)
  if (line.charAt(0) === '$') {
    if (line.includes('cd')) {
      const name = line.split(' ')[2]
      if (currentNode === undefined) {
        currentNode = new Node(name,0, false);
        rootNode = currentNode;

      } else {
        if (name === '..') {
          currentNode = currentNode.parent;
          console.log("Going parent:" + currentNode)
          console.dir(currentNode)
        } else {
          //TODO go to child
          currentNode = currentNode.findChild(name)
          console.log("Going folder: " + currentNode)
          console.dir(currentNode)
        }
      }
    } else if (line.includes('ls')) {
        // do nothing?
      console.log("ls")
      console.dir(currentNode)
    }
  } else {
    const values = line.split(' ');
    if (values[0] === 'dir') {
      const folder = new Node(values[1], 0, false)
      currentNode.addChild(folder);
      folder.parent = currentNode;
      console.log("Adding folder:" + folder)
      console.dir(currentNode)
    } else {
      const size = parseInt(values[0]);
      const file = new Node(values[1], size, true);
      currentNode.addChild(file);
      file.parent = currentNode;
      currentNode.updateSize(size);
      console.log("Adding file:" + file)
      console.dir(currentNode)
    }
  }
}
console.dir(rootNode, { depth: null })

function collectFolderSize(node: Node, limit: number): number {
  let currentSize = 0;
  if (node.folder) {
    currentSize = node.size <= limit ? node.size : 0;
  }
  return currentSize + node.children.map(e => collectFolderSize(e, limit));
}

const result1 = collectFolderSize(rootNode, 100_000);
console.log(result1)