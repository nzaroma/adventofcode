//to compile use this from terminal .\node_modules\.bin\tsc.cmd .\types.ts
// then to run node .\1_types.js
const str: string = 'Hello2';
console.log(str);

const isFetching: boolean = true
const isLoading: boolean = false

let int: number = 33
int = 32
console.log(int)
const float: number = 2.3

const message: string = 'Some sctring';

const numberArray: number[] = [1, 1, 2, 3, 5, 8, 13]
const numberArray2: Array<number> = [1, 1, 2, 3, 5, 8, 13]

const words: string[] = ['Hello', 'lol'];
const words2: Array<string> = ['Hello', 'lol'];

const contact: [string, number] = ['Roman', 70021]

let variable: any = 221
variable = 'sdf';
variable = []


function sayMyName(name: string): void {
  console.log(name)
}

sayMyName("Roman")

//never
function throwError(message: string):never {
      throw new Error(message)
}

function inifinite():never {
  while (true) {

  }
}

//Type
type Login = string

const login: Login = 'admin';

type Id = string | number
const id1: Id = 1223
const id2: Id = 'sdf'
let id3: Id = 23;
id3 = 'ada'

type SomeType = string | null | undefined



