//.\node_modules\.bin\tsc.cmd .\2_interfaces.ts
//node .\2_interfaces.js
interface Rect {
  readonly id: string
  color?: string
  size: {
    width: number
    height: number
  }
}

const rect1 : Rect = {
  id: 'id1',
  size: {
    width: 22,
    height: 32
  },
  color: '#ccc'
}

const rect2 : Rect = {
  id: 'id2',
  size: {
    width: 122,
    height: 132
  }
}
rect2.color = 'black'

const rect3 = {} as Rect
const rect4 = <Rect>{}

interface RectWithArea extends Rect {
  getArea: () => number
}

const rect5: RectWithArea = {
  id: '123',
  size: {
    width: 20,
    height: 21
  },
  getArea(): number {
    return this.size.width * this.size.height;
  }
}

interface IClock {
  time: Date
  setTime(date: Date): void
}

class Clock implements IClock {
  time: Date = new Date()

  setTime(date: Date): void {
    this.time = date
  }

}

interface Styles {
  [key: string]: string
}

const css: Styles= {
  border: '1px',
  marginTop: '2px',
  borderRadius: '5px'
}

