
interface Person {
  name: string
  age: number
}

type PersonKeys = keyof Person // 'name' | 'age'

let myName: PersonKeys = 'age'
myName = 'name'
// myName = 'job'

type User = {
  _id: number
  name: string
  email: string
  createdAd: Date
}

type UserKeysNoMeta = Exclude<keyof User, '_id' | 'createdAd'> // 'name', 'email'
type UserKeysNoMeta2 = Pick<User, 'name' | 'email'>

let u1: UserKeysNoMeta = 'name'
// u1 = '_id'