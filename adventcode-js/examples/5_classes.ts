class Typescript {
  version: string

  constructor(version: string) {
    this.version = version
  }

  info(name: string) {
    return `[${name}]: Typescript version is ${this.version}`
  }
}

const a = new Typescript('hellop')
console.log(a.info('somename'))

// class Car {
//   readonly model: string
//   readonly wheels: number = 4
//
//   constructor(theModel: string) {
//     this.model = theModel;
//   }
//
// }

class Car {
  readonly wheels: number = 4
  constructor(readonly model: string) {
  }
}

class Animal {
  protected voice: string = '';
  public color: string = 'black'

  private go() {
    console.log('Go')
  }
}

class Cat extends Animal {
  public setVoice(voice: string): void {
    this.voice = voice;
  }
}
const cat = new Cat()
// cat.voice
cat.setVoice('test')
console.log(cat.color)

abstract class Component {
  abstract render():void
  abstract info():string
}

class AppComponent extends Component {
  info(): string {
    return 'sfadf'
  }

  render(): void {
  }

}